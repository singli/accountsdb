
CREATE EXTENSION IF NOT EXISTS pgcrypto;
-- Table: public.accounts

DROP TABLE if exists public.accounts cascade;

CREATE TABLE public.accounts
(
  id uuid NOT NULL DEFAULT gen_random_uuid(),
  created_at timestamp without time zone DEFAULT now(),
  username character varying,
  profile jsonb,
  CONSTRAINT accounts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);




-- Table: public.accounts_email
DROP TABLE if exists public.accounts_email;

CREATE TABLE public.accounts_email
(
  address character varying NOT NULL,
  verified boolean NOT NULL,
  user_id uuid NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  arrayidx smallint NOT NULL,
  verification_token character varying,
  vtoken_created_at timestamp without time zone,
  CONSTRAINT accounts_email_pkey PRIMARY KEY (address),
  CONSTRAINT user_id FOREIGN KEY (user_id)
      REFERENCES public.accounts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Index: public.accounts_email_idx_user_id

DROP INDEX if exists public.accounts_email_idx_user_id;

CREATE INDEX accounts_email_idx_user_id
  ON public.accounts_email
  USING btree
  (user_id);



-- Index: public.accounts_username_idx

DROP INDEX if exists public.accounts_username_idx;

CREATE INDEX accounts_username_idx
  ON public.accounts
  USING btree
  (username COLLATE pg_catalog."default");


-- Table: public.account_services_resume

DROP TABLE if exists public.account_services_resume;

CREATE TABLE public.account_services_resume
(
  user_id uuid NOT NULL,
  havelogintokenstodelete boolean DEFAULT false,
  logintokenstodelete jsonb,
  CONSTRAINT account_services_resume_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);


-- Table: public.account_services_password

DROP TABLE if exists public.account_services_password;

CREATE TABLE public.account_services_password
(
  user_id uuid NOT NULL,
  bcrypt character varying NOT NULL,
  resetpending boolean DEFAULT false,
  reset_token character varying,
  reset_at timestamp without time zone,
  CONSTRAINT account_services_password_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);



-- Table: public.account_services_facebook

DROP TABLE if exists public.account_services_facebook;

CREATE TABLE public.account_services_facebook
(
  user_id uuid NOT NULL,
  facebook_id character varying NOT NULL,
  access_token character varying NOT NULL,
  CONSTRAINT account_services_facebook_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);



DROP TABLE if exists public.account_login_tokens;

CREATE TABLE public.account_login_tokens
(
  token character varying NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  order_seq serial NOT NULL,
  user_id uuid NOT NULL,
  CONSTRAINT account_login_tokens_pkey PRIMARY KEY (token),
  CONSTRAINT user_id FOREIGN KEY (user_id)
      REFERENCES public.accounts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Index: public.account_login_tokens_idx_user_id

DROP INDEX if exists public.account_login_tokens_idx_user_id;

CREATE INDEX account_login_tokens_idx_user_id
  ON public.account_login_tokens
  USING btree
  (user_id);

