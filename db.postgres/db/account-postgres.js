'use strict';
import  promise from 'bluebird';
import pg from 'pg-promise';
import pgb from 'pg';
import log from 'loglevel';
import conf from '../config/conf';

var pgp = pg({promiseLib: promise});

// uncomment for verbose debug logs
// log.setLevel('debug');

// note - vital kludge to make timestamp without timezone work properly with JS

var types = pgb.types;
types.setTypeParser(1114, function(stringValue) {
    return stringValue;
});



export default class AccountPostgres {
    constructor() {
        this.cn = conf.get();
        this.db = pgp(this.cn);
        log.debug("in constructor");

        this.serviceMap = {"facebook": "account_services_facebook", "password": "account_services_password"};
    }

    insertUser(fullUser) {
        // deconstruct into fields
        let {profile: pf, username: un, emails: em, services: sv} = fullUser;
        // create bare record with uuid, created-at, and profile
        //noinspection JSUnresolvedVariable

        var acctid;

        if (!(pf) || (Object.keys(pf).length == 0)) {
            log.debug("fixed pf");
            pf = "{}";
        } else {
            pf = JSON.stringify(pf);
        }
        return this.db.one('insert into accounts(profile) values($1) returning id', pf)
            .then( account => {
                log.debug("id is " + account.id);
                acctid = account.id;

                var ops = [];
                ops.push(this.db.any('set transaction isolation level serializable'));

                if (un) {
                    ops.push(this.db.any('update  accounts set username=$1 where id=$2', [un, account.id]));
                }
                if (em) {
                    // TODO: must ensure it is done is sequential order
                    // iterate over array of email adresses
                    let idx = 0;
                    for (let email of em) {
                        ops.push(this.db.any('insert into accounts_email(user_id, address,verified, arrayidx) values($1,$2,$3, $4)',
                            [acctid, email.address, email.verified, idx]));
                        idx = idx + 1;
                        log.debug('ops is length' + ops.length);
                        log.debug('address is ' + email.address);
                    }
                }
                if (sv) {
                    // deconstruct services -- this code changes with each added service
                    let { facebook: fb,  password: pswd }    = sv;


                    if (fb) {
                        ops.push(this.db.any('insert into account_services_facebook(user_id, facebook_id, access_token) values ($1, $2, $3)',
                            [account.id, fb.id, fb.token]));
                    }

                    if (pswd) {
                        ops.push(this.db.any('insert into account_services_password(user_id, bcrypt) values ($1, $2)',
                            [account.id, pswd.bcrypt]));

                    }
                }

                function source(index, data, delay) {
                    if (index < ops.length) {
                        return ops[index];
                    }
                }

                // run all relational field creations in a transaction
                return this.db.tx(function (t) {
                            //t = this
                            return this.sequence(source);
                        })
                    .then(function() {
                        return promise.resolve(acctid);
                    });


            })
             .catch(function (error) {
                log.debug("Postgres ERROR: ", error);

            })
            .finally(function () {
               pgp.end();

            });
    }
    findCurrentUser(userid) {
        return this._findSingleWithProfileUsernameEmail(userid);
    }

    _findSingleWithProfileUsernameEmail(userid) {
        log.debug("find single by uid  with profile, username, email for " + userid);
        return this._coreFinder('id', userid, true);   // shortform search, no services
    }

    findByUsername(username) {
        log.debug("find single by username = " + username);
        return this._coreFinder('username', username);
    }


    findSingle(userid) {
        log.debug("find single with uid = " + userid);
        return this._coreFinder('id', userid);
    }
    _coreFinder(clause, item, shortform) {
        var coll;
        var fb;
        var userid;
        return this.db.one("select json_build_object('id', u.id, 'username', u.username,'profile', u.profile, 'created_at', u.created_at) from accounts u where $1~=$2",[clause, item])
        .then(values => {
            log.debug("values selected " + JSON.stringify(values));
            coll = values.json_build_object;
            userid = coll.id;

            if (coll.username === null) {
                    delete coll.username;
            }
            // ordering is important to reconstruct arrays
            return this.db.any("select address,verified from accounts_email where user_id=$1 order by arrayidx",[userid])
              .then(emails => {
               if (emails) {
                   log.debug("got email " + JSON.stringify(emails));
                   if (emails.length != 0) {
                       coll.emails = emails;
                   }
                  log.debug("collection now (email) " + JSON.stringify(coll));
               }
                  if (shortform) {  // no need for services when searching short form
                      return coll;
                  }
               return this.db.any("select facebook_id as id, access_token as token from account_services_facebook where user_id=$1",[userid])
                  .then(fb => {
                      if (fb.length > 0) {
                          coll.services = {facebook: fb[0]};
                      }
                      log.debug("collection now (facebook) " + JSON.stringify(coll));
                      return this.db.any("select bcrypt from account_services_password where user_id=$1",[userid])
                      .then(pw => {
                          if (pw.length > 0) {
                              if (coll.services) {
                                  coll.services.password = pw[0];
                              } else
                              {
                                  coll.services = {password: pw[0]};
                              }
                          }
                          log.debug("collection now (password) " + JSON.stringify(coll));

                          // internal resume service - ordering is important to reconstruct arrays
                          return this.db.any("select token, created_at  from account_login_tokens where user_id=$1 order by order_seq", [userid])
                             .then(tokens => {
                                 log.debug("tokens found are " + JSON.stringify(tokens));
                                 if (tokens.length > 0) {
                                     var tokensArray = [];
                                     // iterate over tokens
                                     for (let tk of tokens) {

                                         tokensArray.push( {when: {"$date": tk.created_at}, hashedToken: tk.token});
                                     }

                                     if (coll.services) {
                                         coll.services.resume = {loginTokens: tokensArray};
                                     } else {  // the first service
                                         coll.services = {resume: {loginTokens: tokensArray}};
                                     }

                                 }
                                 log.debug("collection now (resume) " + JSON.stringify(coll));
                                 // add resume token to delete information
                                 return this.db.any("select havelogintokenstodelete as flag, logintokenstodelete as tokens from account_services_resume where user_id=$1",[userid])
                                   .then(rows => {
                                     log.debug("tokenstodelete feteched " + JSON.stringify(rows));
                                       if (rows.length > 0) {
                                           if (rows[0].flag) {
                                            if (coll.services) {
                                                if (coll.services.resume) {
                                                    coll.services.resume['haveLoginTokensToDelete'] = true;
                                                    coll.services.resume['loginTokensToDelete'] = rows[0].tokens;
                                                } else {
                                                    coll.services['resume'] = {haveLoginTokensToDelete: true,  logiNTokensToDelete: rows[0].tokens};
                                                }
                                            } else {
                                                coll.services = {resume:  {haveLoginTokensToDelete: true,  logiNTokensToDelete: rows[0].tokens}};
                                            }
                                           }
                                       }
                                       log.debug("collection now (resume after logintokenstodelete) " + JSON.stringify(coll));

                                       // add password service reset token(s)
                                       return this.db.any("select reset_token as token, reset_at as when from account_services_password where user_id=$1",[userid])
                                           .then(rows => {
                                               log.debug("reset_token feteched " + JSON.stringify(rows));
                                               if (rows.length > 0) {
                                                   let tokenRecord = {token: rows[0].token, when: {'$date': rows[0].when}};
                                                   if (rows[0].token) {
                                                       if (coll.services) {
                                                           if (coll.services.password) {
                                                               coll.services.password.reset = tokenRecord;
                                                           } else {
                                                               coll.services.password = {reset: tokenRecord};
                                                           }
                                                       } else {
                                                           coll.services = {password:  {reset:tokentRecord}};
                                                       }
                                                   }
                                               }
                                               log.debug("collection now (resume after reset_token) " + JSON.stringify(coll));

                                               // add email verfication token(s)
                                               return this.db.any("select verification_token as token, vtoken_created_at as when, address from accounts_email where user_id=$1",[userid])
                                                   .then(rows => {
                                                       log.debug("verification tokens found are " + JSON.stringify(rows));
                                                       if (rows.length > 0) {

                                                               var tokensArray = [];
                                                               // iterate over tokens
                                                               for (let row of rows) {
                                                                   if(row.token) {
                                                                       tokensArray.push({
                                                                           when: {"$date": row.when},
                                                                           token: row.token,
                                                                           address: row.address
                                                                       });
                                                                   }
                                                               }
                                                                if (tokensArray.length > 0) {

                                                                    if (coll.services) {
                                                                        if (coll.services.email) {
                                                                            coll.services.email.verificationTokens = tokensArray;
                                                                        } else {
                                                                            coll.services.email = {verificationTokens: tokensArray};
                                                                        }
                                                                    } else {
                                                                        coll.services = {email: {verificationTokens: tokensArray}};
                                                                    }
                                                                }


                                                       }

                                                       log.debug("collection now (resume after verification tokens) " + JSON.stringify(coll));
                                                       return coll;
                                                   });


                                           });

                                   })
                                   .catch(function(err) {
                                       log.debug("ERROR " + err.message);
                                   }) ;

                             })
                            .catch(function(err) {
                               log.debug("ERROR " + err.message);
                            });
                          });

                      });


                  });

            })
        .catch(function(err) {
            log.debug("ERROR " + err.message);
        });

    }

    // user = Meteor.users.findOne({ _id: query.id });

    findByService(svname, serviceid) {
        // always called with selector {services.<servicename>.id: "id"}

        var serviceTable;
        var serviceIdField;
        // check only EXTERNAL services
        if (svname == 'facebook') {
            serviceTable = "account_services_facebook";
            serviceIdField = "facebook_id";
        }
        if (serviceTable) {


            return this.db.any("select user_id from $1~ where $2~=$3",[serviceTable, serviceIdField, serviceid ])
                .then(fetched => {
                    log.debug(JSON.stringify(fetched));
                    return this.findSingle(fetched[0].user_id);

                })
                .catch(function(err) {
                    log.debug("ERR " + err.message);
                });
        }


    }

    //  The following are mostly used in setup of package tests  - via TinyTest

    removeByUsername(username) {
        // delete the user based on username
        // return this.users.remove({username: username});

        //  TODO: setup cascade delete on the table basis
        let deletecascade = promise.coroutine(function* () {
                log.debug('find userid from username');
                let user = yield this.findByUsername(username);

                log.debug('deleting facebook services');
                let res1 = yield this.db.result("delete from account_services_facebook where user_id=$1",[user.id]);
                log.debug('deleting password services');
                let res3 = yield this.db.result("delete from account_services_password where user_id=$1",[user.id]);
                log.debug('deleting login tokens');
                let res4 = yield this.db.result("delete from account_login_tokens where user_id=$1",[user.id]);
                log.debug('deleting account emails');
                let res5 = yield this.db.result("delete from accounts_email where user_id=$1",[user.id]);
                log.debug('deleting accounts');
                let res6 = yield this.db.result("delete from accounts where id=$1",[user.id]);
                log.debug("res6 is " + JSON.stringify(res6));
                if (res6.rowCount != 1) {
                    throw new Error('problem removing by username');
                }
                return res6;
        }.bind(this));

        return deletecascade();

    }

    unsetProfile(userId) {

        // clear out the profile and username fields for the specified userId
        // this.users.update(userId,
        //    {$unset: {profile: 1, username: 1}});
        return this.db.result("update accounts set profile=$1, username=$2 where id=$3",[null, null, userId]);
    }

    findById(userId) {
        return this.findSingle(userId);
    }



    // working with login tokens

    expireTokens(oldestValidDate, userId) {
        // for a specified user, given the oldest valid date, expire all older token
   log.debug('oldestValideDate is ' + JSON.stringify(oldestValidDate));
        // if userId is null, expire token for everyone on system
        var deleteStmt;
        if (userId) {
          deleteStmt = "delete from account_login_tokens where created_at<$1 and user_id=$2";
        } else {
            log.debug("no userid specified!!");
            deleteStmt =  "delete from account_login_tokens where created_at<$1";
        }

        return this.db.result(deleteStmt, [oldestValidDate, userId]);

    }


    clearAllLoginTokens(userId) {
        // delete all login token for a user
        return this.db.none("delete from account_login_tokens where user_id=$1", [userId]);

    }

    insertHLoginToken(userId, hashedToken, qry) {
        // log.debug("called insert login token " + userId + " --- " + JSON.stringify(hashedToken) + " query is " + JSON.stringify(qry));

        // insert a login token for a user

        return this.db.one('insert into account_login_tokens (user_id, token) values($1, $2)', [userId, hashedToken])
            .then(val => {
                log.debug(JSON.stringify(val));
                return 'ok';

            })
            .catch(function (err) {
                log.debug("ERR " + err.message);
            });
    }

    findUserWithNewtoken(hashedToken) {
        // find the user with a specified token

        return this.db.one('select user_id from account_login_tokens where token=$1', [hashedToken])
            .then(res => {
                log.debug("findUserWithNewToken with userid " + res.user_id);
                return this.findSingle(res.user_id);

            })
        .catch(function (err) {
           log.debug("ERR " + err.message);
        });

    }

    findUserWithNewOrOld(newToken, oldToken) {
        // old token method
        // DO NOT support old token
        /*
        return  this.users.findOne({
            $or: [
                {"services.resume.loginTokens.hashedToken": newToken},
                {"services.resume.loginTokens.token": oldToken}
            ]
        });
        */
        if (oldToken) {
            var status = "WARNING:  findUserWithNewOrOld invoked with oldToken but not implemented";
            throw new Error(status);
        }
        return this.findUserWithNewtoken(newToken);
    }



    deleteSavedTokens(userId, tokensToDelete) {
        log.debug("DEL SAVED TOK " + JSON.stringify(tokensToDelete));
        // tokensToDelete must be an array of tokens
        if (tokensToDelete) {
            // reset haveloginTokensToDelete and loginTokensToDelete
            return this.db.result('update  account_services_resume set haveLoginTokensToDelete=$1, loginTokensToDelete=$2::jsonb where user_id=$3', [false,'[]', userId])
                .then( () => {
                    log.debug("tokens to delete are " + JSON.stringify(tokensToDelete));
                    // delete all the specified login Tokens for the user
                   return this.db.result('delete from account_login_tokens where user_id=$1  and token= any( $2::varchar[] )', [userId, tokensToDelete]);
                })
                .catch(function (err) {
                    log.debug("ERROR " + err.message);
                });
        }
    }

    deleteSavedTokensforAllUsers() {
        // for each user with haveLoginTokensToDelete set to true
        // delete the list of tokens stored in loginTokensToDelete

        return this.db.any('select user_id, loginTokensToDelete from account_services_resume where haveLoginTokensToDelete=true')
         .then(users => {
             log.debug("USERS with loginTokensToDelete " + JSON.stringify(users));
             var ops = [];
             for (let user of users) {
                 ops.push(this.deleteSavedTokens(user.user_id, user.logintokenstodelete));
             }

             // delete multi users within a transaction
             return this.db.tx(function (t) {
                 //t = this
                 return this.batch(ops);
             });
         });

    }
    removeOtherTokens(userId, curToken) {
        // delete all other tokens for a user (excpect the specified curToken)
        if (!userId) {
            throw new Error("You are not logged in.");
        }
        return this.db.none("delete from account_login_tokens where user_id=$1 and token<>$2",[userId, curToken]);
    }

    getNewToken(userId, curToken, newToken) {

        if (!userId) {
            throw new Error("You are not logged in");
        }


        // Be careful not to generate a new token that has a later
        // expiration than the curren token. Otherwise, a bad guy with a
        // stolen token could use this method to stop his stolen token from
        // ever expiring.

        // here, we do this by keeping the timestamp from the current token - and replace just the token string
        return this.db.result('update account_login_tokens set token=$1 where user_id=$2 and token=$3', [newToken, userId, curToken])
          .then(result => {
                if (result.rowCount === 0) {
                    throw new Error("Invalid login token");
                }
              return this.db.one('select created_at from login_tokens where token=$1', [newToken])
              .then(timecreated => {
                  // return the new token
                  var tok = {};
                  tok.when = {"$date": timecreated};
                  tok.hashedToken = newToken;
                  return tok;

              })
              .catch(function(err) {
                  log.debug("ERR " + err.message);
              });

          })
        .catch(function(err) {
            log.debug("ERR " + err.message);
        });

    }


    addNewHashedTokenIfOldUnhashedStillExists(userId, oldToken, newHashedToken, when) {
        //  log.debug("called addNewHasedTokenIfOldUnhashedStillExists with userID " + userId + "oldToken= " + oldToken + "newToken =" + newHashedToken +  "when = " + when);
        // working with old tokens -- not implemented

        /*

        this.users.update({_id: userId,
                "services.resume.loginTokens.token": oldToken},
            {
                $addToSet: {
                    "services.resume.loginTokens":
                    {
                        "hashedToken":  newHashedToken,
                        "when":when
                    }
                }});
                */

        var status = "WARNING:  addNewHashedTokenIfOldUnhashedStillExists is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);

    }


    removeOldTokenAfterAddingNew(userid, oldToken) {
        // log.debug("called removeOldTokenAfterAddingNew with userID " + userid + "oldToken= " + oldToken);
        /*
        this.users.update(userid, {
            $pull: {
                "services.resume.loginTokens": {"token": oldToken}
            }
        });
        */
        var status = "WARNING:  removeOldTokenAfterAddingNew is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);
    }

    destroyToken(userId, loginToken) {
        // remove specified token for specified userId

        return this.db.none("delete from account_login_tokens where user_id=$1 and token=$2", [userId, loginToken]);

    }

    findSingleLoggedIn(userId) {
        // find the logged in user

        // if the sql service provider is coded correctly, this direct-from-mongo-code method
        // should "just work"

       return this.findSingle(userId)
        .then(user => {
            // for the lack of an existantial operator :(
            if(user) {

                if(user.services) {
                    if (user.services.resume) {
                        if (user.services.resume.loginTokens) {
                            return promise.resolve(user);
                        } else {
                            return promise.resolve(null);
                        }
                    } else {
                        return promise.resolve(null);
                    }

                } else {
                    return promise.resolve(null);
                }

            } else {  //null
                return promise.resolve(user);
            }


        })
        .catch(function(err) {
            log.debug("ERR " + err.message);
        });

    }

    saveTokenAndDeleteLater(userId, tokens, hashedStampedToken) {
        // For a specified userId,
        // tokens contains an array of tokens to delete later (token strings only)
        // hashedStampedToken is a token to be inserted into the loginTokens list now, creating it if not existing yet


        // TODO: need to combine the two ops below into one transaction
        // implemented as an upsert into the accout_services_resume table

        // nb -> upsert syntax is new and tricky
        return this.db.result('insert into  account_services_resume (user_id, havelogintokenstodelete, logintokenstodelete) values  ( $1, $2, $3) on conflict (user_id) do update  set havelogintokenstodelete=$2, logintokenstodelete=$3', [userId,true, JSON.stringify(tokens)])
            .then(res => {
                if (res.rowCount > 0) {
                    // updated
                    this.db.result('insert into account_login_tokens (user_id, token) values ($1, $2)', [userId, hashedStampedToken])
                    .then(result => {
                       if (result.rowCount > 0) {
                           return true;
                       }
                        return false;
                    })
                    .catch(function(err) {
                        log.debug("ERR " + err.message);
                    });
                }

            })
        .catch(function(err) {
           log.debug("ERR " + err.message);
        });


    }

    // password_server requirements

    findByResetToken(tokenRec) {
        // returns the user with the supplied password reset token
        // return this.users.findOne({"services.password.reset.token": token});

        return this.db.one('select user_id from account_services_password where reset_token=$1', [tokenRec.token])
            .then(result => {
                log.debug("res is " + JSON.stringify(result));
                return this.findSingle(result.user_id);

            })
            .catch(function(err) {
                log.debug("ERR " + err.message);

            });

    }

    findByVerificationTokens(tokenRec) {
        // returns the user with the supplied email verification token
       // return this.users.findOne({"services.email.verificationTokens.token": token});

        log.debug("FIND BY VERIFICATIONTOKENS " + JSON.stringify(tokenRec));
        return this.db.one('select user_id from accounts_email where verification_token=$1 and address=$2', [tokenRec.token, tokenRec.address])
            .then(result => {
                return this.findSingle(result.user_id);

            })
            .catch(function(err) {
                log.debug("ERR " + err.message);

            });

    }

    updateToBcrypt(userid, salted) {
        // working with old SRP passowrds -- not implemented
        /*
        this.users.update(userid, {$unset: { 'services.password.srp': 1 }, $set: { 'services.password.bcrypt': salted }});
        */

        var status = "WARNING:  updateToBcrypt is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);

    }

    setUsername(userid, username) {
        // change the username for a specified user
        // this.users.update({_id: userid}, {$set: {username: username}});

        return this.db.none('update  accounts set username=$1 where id=$2', [username, userid]);

    }

    replaceAllTokensOtherThanCurrentConnection(userId, hashed, currentToken) {
        // for specified user, set the encrypted password, remove all tokens except the currentToken, AND clear the reset status
        return this.db.result('insert into  account_services_password (bcrypt, user_id) values ($1,$2)   on conflict (user_id) do update set bcrypt=$1, user_Id=$2', [hashed, userId])
           .then(res => {
               this.removeOtherTokens(userId, currentToken)
               .then(result => {
                  return  this.db.none('update account_services_password set resetpending=false');

               });
           });


    }

    changePasswordResetVerifyEmail(userid, email, token, hashed) {
        // for a specified user, email, and reset token combination - clear resetpending, set the bcrypt password, and set verified to true for corresponding email
        log.debug("TOKEN IN is " + JSON.stringify(token));

        let tokenRec = {'token': token};
        // TODO: verify if higher level logic expects to reset anything else other than primary email
        // TODO: put all in a tx
        return this.findByResetToken(tokenRec)
          .then(userfound => {
                    if (userfound.id !=  userid) {
                        throw new Error("token does not belong to user");
                    }
                    return this.db.none('update account_services_password set bcrypt=$1, resetpending=false, reset_token=null where user_id=$2', [hashed, userid])
                     .then(() => {
                        return this.setEmailVerified(userid, email);
                     });
          });

    }

    setVerificationTokens(userId, tokenRecord) {
        //set the verification token for a user
        /* this.users.update(
            {_id: userId},
            {$push: {'services.email.verificationTokens': tokenRecord}}); */
        return this.db.result('update  accounts_email set verification_token=$1,vtoken_created_at=now() where user_id=$2 and address=$3', [tokenRecord.token, userId, tokenRecord.address]);

    }

    setResetToken(userId, tokenRecord) {
        // set the password reset token for a user
        /*
        this.users.update(userId, {
            $set: {
                "services.password.reset": tokenRecord
            }
        });
        */
        return this.db.result('update  account_services_password set resetpending=$1,reset_token=$2,reset_at=now() where user_id=$3', [true, tokenRecord.token, userId]);
    }

    setEmailVerified(userid, addr) {
        // for a sepcified user id,  find the email - set verified status to true and remove the associated verification token
        /*
        this.users.update({_id: userid, 'emails.address': addr},
            {
                $set: {'emails.$.verified': true},
                $pull: {'services.email.verificationTokens': {address: addr}}
            });
            */
        // TODO:  verify if higher level logic expects to set anything but the primary email
        return this.db.result('update accounts_email set verified=true, verification_token=null where user_id=$1',[userid, addr]);

    }




    addEmailVerified(userId, email, verified) {
        // for a specified user id, find the email - set verified status
        /*
        this.users.update(
            {_id: userId},
            {$push: {emails: {address: email, verified: verified}}});
        */
        // note verified can be true or false
        // TODO:  may have to convert this to an upsert
        return this.db.result('insert into accounts_email (verified, user_id, address, arrayidx) values ($1,$2,$3, (select (max(arrayidx) + 1)  from accounts_email where user_id=$4)) on conflict (address) do update set verified=$1',[verified, userId, email, userId])
        .then(function(res) {
           if (res.rowCount != 1) {
               throw new Error('addEmailVerified - cannot update verfied field for ' + email);
           }
            return true;
        });


    }

    setNewEmailVerified(userid, email, newEmail, verified) {

        // replace an existing email with a new email and verification status
        /*
        this.users.update({_id: userid, 'emails.address': email},
            {
                $set: {
                    'emails.$.address': newEmail,
                    'emails.$.verified': verified
                }
            });
            */
        // note we're changing the primary key value here

       return this.db.result('update accounts_email set address=$1, verified=$2 where user_id=$3 and address=$4',[newEmail, verified, userid, email]);
    }


    addEmailAddressVerified(userid, newEmail, verified) {

        // add an email and verification status record
        /*
        this.users.update({_id: userid},
            {
                $addToSet: {
                    emails: {
                        address: newEmail,
                        verified: verified
                    }
                }
            });
            */
       return  this.addEmailVerified(userid, newEmail, verified);
    }

    forceChangePassword(userid, hashedPassword, loggedout) {

        /*
        var update = {
            $unset: {
                'services.password.srp': 1, // XXX COMPAT WITH 0.8.1.3
                'services.password.reset': 1
            },
            $set: {'services.password.bcrypt': hashedPassword }
        };

        if (loggedout) {
            update.$unset['services.resume.loginTokens'] = 1;
        }

        this.users.update({_id: userid}, update);
        */
        // for a specified user, set the password and make sure resetpending is false;  also clear loginTokens if loggedOut

        if (loggedout) {
            this.clearAllLoginTokens(userid)
            .then(res => {
               return this.db.result("update account_services_password set bcrypt=$1, resetpending=false where user_id=$2",[hashedPassword, userid] );
            });
        }
        return  this.db.result("update account_services_password set bcrypt=$1, resetpending=false where user_id=$2",[hashedPassword, userid] );
    }

    undoEmailAddressUpdate(userid, newEmail) {
        // remove the email address specified

         // this.users.update({_id: userid}, {$pull: {emails: {address: newEmail}}});

        return this.db.result("delete from accounts_email where user_id=$1 and address=$2",[userid,newEmail]);
    }

    setSRP(userId, identity, salt, verifier) {
        // working with old SRP passowrds -- not implemented
        /*
         this.users.update(userid, {$unset: { 'services.password.srp': 1 }, $set: { 'services.password.bcrypt': salted }});
         */
        /*
        this.users.update(
            userId,
            {
                '$set': {
                    'services.password.srp': {
                        "identity": identity,
                        "salt": salt,
                        "verifier": verifier
                    }
                }
            });
        */
        var status = "WARNING:  setSRP() is invoked but not implemented";
        log.debug(status);
        return status;

    }

    resetCollection() {
        log.debug('reset Collection!');
        // use postgres optimized cascade delete extension
        return this.db.none('truncate table account_login_tokens, account_services_resume, account_services_facebook, account_services_password, accounts_email, accounts cascade');

    }

}


