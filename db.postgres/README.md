# (Meteor Accounts) Postgresql Database Provider Explorer's Kit


#### Quick Start

```
git clone https://bitbucket.org/singli/accountsdb.git
cd db.postgres
npm install
```

Then edit the `config/conf.js` to point to your Postgresql 9.5 (or higher) server, with a database that you have write permission to.

Create the tables in your Postgres instance by:

```
npm start
```

Now run the unit tests against your (Meteor Accounts) Postgres database provider implementation.

```
npm test
```

To modify the schema used by the provider, edit the file `schema/createtable.sql`.

To modify the Postgresql database provider, edit the file `db/account-postgres.js`.

To see the current unit tests, or to add unit tests, edit the file `test/test.js`.

#### Technology Stack

* designed for rapid iterations during data provider development; facilitates experimentation
* completely decoupled from Meteor for data provider development
* provider code in ES2015 aka ES6 via Babel transpiler
* bluebird.js promise
* mocha for all unit tests
* provider code is npm module-ready

#### Developer tips

To see verbose debug log when unit tests are running, uncomment the `log.setLevel(debug)` line at the top of a module.

To run just one single test from the command line:

```
mocha --compilers js:babel-core/register -g '<name of method to test>'
```

#### Contributors wanted

* add more unit tests
* tune the provider for performance
* refine schema used
* fix bugs
* implement ORACLE SQL provider
* implement MySQL provider
* implement MariaDB provider
* implement DB2 provider
* implement Microsoft SQL Server provider
* implement Informix provider

# Background

* Meteor is growing up - adapting to the requirements of users with very large and advanced applications (major level of investment)
* traditional Meteor is tightly coupled with Mongo as a data provider and depends on its oplogs for reactivity
* new Meteor will work with SQL, NoSQL, REST, and other data sources (even simultaneously); reativity will be separately implemented where required - faciliated by [Apollo](https://github.com/apollostack/apollo)
* transition is evolutionary - Incrementally, Side-by-side (MattD, Feb 2016)

See Meteor's founders' talks for more background information:

*   [Meteor 1.3 and Beyond - Matt DeBergalis, Feb 23, 2016](https://www.youtube.com/watch?v=7d0xTR-eYh0 )
*   [Transmission - Future of Meteor with CEO Geoff Schmidt, March 2, 2016]( https://www.youtube.com/watch?v=wylWt-RxYiQ)

Read about the [Apollo stack and its mission](https://github.com/apollostack/apollo).

## Refactoring Accounts
* aim to separate the Meteor Accounts modules into its own repository (similar to Blaze)
* implement (Postgresql) SQL provider for Accounts  (without reativity)

### Approach 
1. Phase 1 - separate the monolithic Mongo code into application code calling through a _model layer_
2. Define the Database Access API based on the _model layer_ in the form of [a set of methods](https://github.com/RocketChat/meteor/blob/f9df7a91444ff3fd558996d7a6c198306571c2ab/packages/accounts-base/model_users.js)
3. Phase 2 - implement the [set of Database Access API methods in Postgreql](https://bitbucket.org/singli/accountsdb/src/219fc8a1e3a5a548c15b87d2dded92edea276c0e/db.postgres/db/account-postgres.js?at=master&fileviewer=file-view-default); as an example of a SQL provider; create unit tests and a development/experimentation environment 
4. Phase 2 - implement the [set of Database Access API methods in RethinkDB](https://bitbucket.org/singli/accountsdb/src/219fc8a1e3a5a548c15b87d2dded92edea276c0e/db.rethink/db/account-rethink.js?at=master&fileviewer=file-view-default); as an example of a NoSQL provider; create unit tests and a development/experimentation environment
5. Phase 2 - develop and refine the [single unit tests suite](https://bitbucket.org/singli/accountsdb/src/219fc8a1e3a5a548c15b87d2dded92edea276c0e/db.postgres/test/test.js?at=master&fileviewer=file-view-default) running against 3, 4, and other (community contributed) data source providers 
6. Phase 3 - revisit and fine tune the Database Access APIs to cater for [Apollo](https://github.com/apollostack/apollo)


### Why Accounts?
* Accounts (the users collection) is the primary link between a Meteor application and a specific data source (Mongo thus far)
* outside of the Accounts linkage, a Meteor application - especially new ones - can have a wide choice of data providers other than Mongo
* having multiple data providers for Accounts can serve as example for users transitioning their exsiting Metor application to non-Mongo data sources

#### Why would I ever want to write my own database provider for Accounts?
* traditioally, it was the job of Meteor's creator (MDG) since there was only one provider -- MongoDB -- and highly engineered, bundled reactivity
* with the new Meteor, it is very likely that you want to customize your own for the best performance
* the Accounts (users) related data may already be available in your organization, but not in a form supported by current providers
* even with SQL providers, you may want to customize the schema and data access to BEST FIT your existing system
* you may have mixed data provider requirements

