import  chai from  'chai';
import  accounts from '../db/account-postgres';
import "babel-polyfill";
import  bluebird from 'bluebird';
import log from 'loglevel';

/*
 uncomment for verbose debug logs
 log.setLevel('debug');
 */
var assert = chai.assert;
let users = new accounts();

var _throwCheckedExceptions;
_throwCheckedExceptions = function (err) {
    log.debug("error is " + JSON.stringify(err));
    if (err && err.name === "ReferenceError") {
        throw err;
    }
    if (err && err.name === "error") {
        throw err;
    }

    // other errors that should throw exceptions
};

var _assertDeepEqual;
_assertDeepEqual = function (a, b) {

    // remove all generated UUID and timestamp fields (which will always be different)  before comparision
    if (b.id) {
        delete b.id;
    }

    if (b.created_at) {
        delete b.created_at;
    }
    if (a.id) {
        delete a.id;
    }

    if (a.created_at) {
        delete a.created_at;
    }

    if (a.services) {

        if (a.services.resume) {
            for (let logTok of a.services.resume.loginTokens) {
                delete logTok.when;
            }
        }
        if (a.services.password) {
            if (a.services.password.reset) {
                delete a.services.password.reset.when;
            }
        }
        if (a.services.email) {
            if (a.services.email.verificationTokens) {

                for (let verTok of a.services.email.verificationTokens) {
                    delete verTok.when;
                }
            }
        }
    }
    if (b.services) {

        if (b.services.resume) {
            for (let logTok of b.services.resume.loginTokens) {
                delete logTok.when;
            }
        }
        if (b.services.password) {
            if (b.services.password.reset) {
                delete b.services.password.reset.when;
            }
        }
        if (b.services.email) {
            if (b.services.email.verificationTokens) {

                for (let verTok of b.services.email.verificationTokens) {
                    delete verTok.when;
                }
            }
        }
    }
    log.debug("assert A " + JSON.stringify(a));
    log.debug("assert B " + JSON.stringify(b));
    return assert.deepEqual(a, b);
};


describe('Accounts', function() {

    beforeEach(function (done) {
        return users.resetCollection()
            .then(function () {
                done();
            });
    });


    describe('insertFullUser', function () {

        it('should insert basic user just profile no email, username, or services', function (done) {
            let user = {profile: {}};
            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });
        it('should insert basic user profile and username, no email or services', function (done) {
            let user = {username: "johnny", profile: {}};
            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });
        it('should insert user with email, no services', function (done) {
            let user = {username: "jack", emails: [{address: "jack@jack.com", verified: false}], profile: {}};
            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with multiple email addresses, no services', function (done) {
            let user = {
                username: "mark",
                emails: [{address: "mark@jack.com", verified: false}, {address: "mark@mark.com", verified: true}],
                profile: {}
            };
            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with no email, with password service', function (done) {
            let user = {username: "mary", services: {password: {'bcrypt': 'xxxooo323232'}}, profile: {}};
            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with  email, with password service', function (done) {
            let user = {
                username: "mary",
                emails: [{'address': 'mary@mary.com', verified: false}],
                services: {password: {'bcrypt': 'io23i2o32323'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with multiple email addresses, with password service', function (done) {
            let user = {
                username: "jackie",
                emails: [{'address': 'jackie@mary.com', verified: false}, {
                    'address': 'sister@mary.com',
                    verified: false
                }],
                services: {password: {'bcrypt': '93843433'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with no email, with facebook service', function (done) {
            let user = {
                username: "sally",
                services: {facebook: {'id': 'sallysally3232', 'token': 'lslsdjwjeewwe'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with  email, with facebook service', function (done) {
            let user = {
                username: "josh",
                emails: [{'address': 'josh@mary.com', verified: true}],
                services: {facebook: {'id': 'joshbigjosh', 'token': 'xxoiwewxx'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with multiple email addresses, with facebook service', function (done) {
            let user = {
                username: "martha",
                emails: [{'address': 'martha@mary.com', verified: false}, {
                    'address': 'boldmartha@mary.com',
                    verified: true
                }],
                services: {facebook: {'id': 'marthabigwart', 'token': '392392323'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with no email, with facebook and password services', function (done) {
            let user = {
                username: 'molly',
                services: {facebook: {'id': 'molyduck', 'token': 'i3u2i3232'},
                password: {bcrypt: '83929328'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });


        it('should insert user with  email, with facebook and password services', function (done) {
            let user = {
                username: "walter",
                emails: [{'address': 'walter@walt.com', verified: true}],
                services: {facebook: {'id': 'kenflyer32323', 'token': 'xxioadasadas'},
                    password: {bcrypt: 'owiewiewo333333'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });

        it('should insert user with multiple email addresses, with facebook and password services', function (done) {
            let user = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                services: {facebook: {'id': 'kenflyer32323', 'token': 'xxioadasadas'},
                password: {bcrypt: 'owiewiewo333333'}},
                profile: {}
            };

            users.insertUser(user)
                .then(function (userid) {
                        users.findSingle(userid)
                            .then(function (fetched) {
                                _assertDeepEqual(user, fetched);
                                done();
                            }.bind(this));
                    }.bind(this)
                );
        });
        
    });


    // find by username only used in account unit tests
    describe('findByUsername', function () {
        it('should find basic user just profile no email, services', function (done) {
            let user1 = {username: "johnny", profile: {name: 'johnny'}};
            let user2 = {username: "lily", profile: {name: 'lily'}};
            users.insertUser(user1)
                .then(function (userid) {
                    users.insertUser(user2)
                    .then(function (userid) {
                        users.findByUsername('lily')
                        .then(function(fetched) {
                            _assertDeepEqual(user2, fetched);
                            done();
                        }.bind(this));
                    }.bind(this));
                }.bind(this));

        });
        it('should find basic user with others with null usernames', function (done) {
            let user1 = {profile: {name: 'johnny'}};
            let user2 = {username: "lily", profile: {name: 'lily'}};
            users.insertUser(user1)
                .then(function (userid) {
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findByUsername('lily')
                                .then(function(fetched) {
                                    _assertDeepEqual(user2, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));

        });

        it('should find full user with emails and services', function (done) {
        let user1 = {
            username: "ken",
            emails: [{'address': 'ken@mary.com', verified: false}, {
                'address': 'flyerken@ken.com',
                verified: true
            }],
            services: {facebook: {'id': 'kenflyer32323', 'token': 'xxioadasadas'},
                password: {bcrypt: 'owiewiewo333333'}},
            profile: {testing: true}
        };
            let user2 = {username: "lily", emails: [{address:'lily@lily.com', verified: false}],
                services: {facebook: {'id': 'lilytheflower3232', 'token': '93939393'},
                    password: {bcrypt: 'kdkdkdkdkdkdkdkdkd'}},
                profile: {name: 'lily'}};
            users.insertUser(user1)
                .then(function (userid) {
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findByUsername('ken')
                                .then(function(fetched) {
                                    _assertDeepEqual(user1, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));
            });
    });

    describe('findCurrentUser', function () {
        it('should find basic user just profile no username no email, services', function (done) {
            let user1 = {username: "johnny", profile: {name: 'johnny'}};
            let user2 = {profile: {name: 'lily'}};
            users.insertUser(user1)
                .then(function (userid) {
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findCurrentUser(userid)
                                .then(function(fetched) {
                                    _assertDeepEqual(user2, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));

        });

        it('should find basic user with profile and username no email, services', function (done) {
            let user1 = {username: "johnny", profile: {name: 'johnny'}};
            let user2 = {username: "lily", profile: {name: 'lily'}};
            users.insertUser(user1)
                .then(function (userid) {
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findCurrentUser(userid)
                                .then(function(fetched) {
                                    _assertDeepEqual(user2, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));

        });




        it('should find full user with emails and services', function (done) {
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                services: {facebook: {'id': 'kenflyer32323', 'token': 'xxioadasadas'},
                    password: {bcrypt: 'owiewiewo333333'}},
                profile: {testing: true}
            };
            let user1Trimmed = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };


            let user2 = {username: "lily", emails: [{address:'lily@lily.com', verified: false}],
                services: {facebook: {'id': 'lilytheflower3232', 'token': '93939393'},
                    password: {bcrypt: 'kdkdkdkdkdkdkdkdkd'}},
                profile: {name: 'lily'}};
            var user1id;
            users.insertUser(user1)
                .then(function (userid) {
                    user1id  = userid;
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findCurrentUser(user1id)
                                .then(function(fetched) {
                                    _assertDeepEqual(user1Trimmed, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));
        });
    });



    // find by service
    describe('findByService', function () {
        it('should find by service id when other has no service', function (done) {
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };



            let user2 = {username: "lily", emails: [{address:'lily@lily.com', verified: false}],
                services: {facebook: {'id': 'lilytheflower3232', 'token': '93939393'},
                    password: {bcrypt: 'kdkdkdkdkdkdkdkdkd'}},
                profile: {name: 'lily'}};
            var user1id;
            users.insertUser(user1)
                .then(function (userid) {
                    user1id  = userid;
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findByService('facebook', 'lilytheflower3232')
                                .then(function(fetched) {
                                    _assertDeepEqual(user2, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));
        });

        it('should find by service id when other has same service', function (done) {
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                services: {facebook: {'id': 'kenflyer32323', 'token': 'xxioadasadas'},
                    password: {bcrypt: 'owiewiewo333333'}},
                profile: {testing: true}
            };



            let user2 = {username: "lily", emails: [{address:'lily@lily.com', verified: false}],
                services: {facebook: {'id': 'lilytheflower3232', 'token': '93939393'},
                    password: {bcrypt: 'kdkdkdkdkdkdkdkdkd'}},
                servicesprofile: {name: 'lily'}};
            var user1id;
            users.insertUser(user1)
                .then(function (userid) {
                    user1id  = userid;
                    users.insertUser(user2)
                        .then(function (userid) {
                            users.findByService('facebook', 'kenflyer32323')
                                .then(function(fetched) {
                                    _assertDeepEqual(user1, fetched);
                                    done();
                                }.bind(this));
                        }.bind(this));
                }.bind(this));
        });

    });

    // resume internal services tests

    // insertHLoginToken
    describe('insertHLoginToken', function () {
        it('should add resume service with token', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

           var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            users.insertUser(user1)
            .then(function(userid) {
                users.insertHLoginToken(userid, tokenval, {})
                .then(function(result) {
                    users.findSingle(userid)
                    .then(function(fetched) {
                        _assertDeepEqual(user1LoggedIn, fetched);
                        done();
                    });


                }.bind(this));
            }.bind(this));


        }.bind(this));

        });  // of insertHLoginToken


    // findUserWithNewToken
    describe('findUserWithNewToken', function () {
        it('should find user just logged in', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

            var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            // user coroutine to simplify logic
            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting isnertHLoginToken');
                let result = yield users.insertHLoginToken(userid, tokenval, {});
                log.debug('starting findUserWithNewToken');
                let fetched = yield users.findUserWithNewtoken(tokenval);
                _assertDeepEqual(user1LoggedIn, fetched);
                done();

            });

            runtest(done);

        });

    });  // of findUserWithNewToken


    // destroyToken
    describe('destroyToken', function () {
        it('should destory token after logging in', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

            var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            users.insertUser(user1)
                .then(function(userid) {
                    users.insertHLoginToken(userid, tokenval, {})
                        .then(function(result) {
                            users.findUserWithNewtoken(tokenval)
                                .then(function(fetched) {
                                    // confirm token inserted
                                    _assertDeepEqual(user1LoggedIn, fetched);
                                    users.destroyToken(userid, tokenval)
                                    .then(function() {
                                         users.findSingle(userid)
                                        .then(function(fetched) {
                                            _assertDeepEqual(user1, fetched);
                                            done();
                                        }.bind(this));

                                    }.bind(this));

                                }.bind(this));


                        }.bind(this));
                }.bind(this));



        }.bind(this));

        it('should do nothing if token does not exist', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

            var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            users.insertUser(user1)
                .then(function(userid) {
                    users.insertHLoginToken(userid, tokenval, {})
                        .then(function(result) {
                            users.findUserWithNewtoken(tokenval)
                                .then(function(fetched) {
                                    // confirm token inserted
                                    _assertDeepEqual(user1LoggedIn, fetched);
                                    users.destroyToken(userid, "nosuchtoken")
                                        .then(function() {
                                            users.findSingle(userid)
                                                .then(function(fetched) {
                                                    _assertDeepEqual(user1LoggedIn, fetched);
                                                    done();
                                                }.bind(this));

                                        }.bind(this));

                                }.bind(this));


                        }.bind(this));
                }.bind(this));



        }.bind(this));



    });  // of destroyToken



    // removeOtherTokens
    describe('removeOtherTokens', function () {
        it('should remove all other token except first', function (done) {
            let token1val = "mytoken123";
            let token2val = "my2ndtoken";
            let token3val = "my3rdtoken";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };

            var user1SingleToken = Object.assign({}, user1);
            user1SingleToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}]}};

            var user1MultiToken = Object.assign({}, user1);
            user1MultiToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val} ]}};


            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting first isnertHLoginToken');
                let result1 = yield users.insertHLoginToken(userid, token1val, {});
                log.debug('starting second isnertHLoginToken');
                let result2 = yield users.insertHLoginToken(userid, token2val, {});
                log.debug('starting third isnertHLoginToken');
                let result3 = yield users.insertHLoginToken(userid, token3val, {});
                log.debug('starting findSingle');
                let fetched = yield users.findSingle(userid);
                // added three tokens
                _assertDeepEqual(user1MultiToken, fetched);
                let result4 = yield users.removeOtherTokens(userid, token1val);
                let removed  = yield users.findSingle(userid);
                // removed all other tokens
                _assertDeepEqual(user1SingleToken, removed);
                done();

            });

            runtest(done);
        });

    });  // of removeOtherTokens

    // clearAllLoginTokens
    describe('clearAllLoginTokens', function () {
        it('should remove all tokens', function (done) {

            let token1val = "mytoken123";
            let token2val = "my2ndtoken";
            let token3val = "my3rdtoken";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };

            var user1SingleToken = Object.assign({}, user1);
            user1SingleToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}]}};

            var user1MultiToken = Object.assign({}, user1);
            user1MultiToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val} ]}};




            let runtest = bluebird.coroutine(function* (done) {
                try {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('starting first isnertHLoginToken');   // order matters in token array
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});
                    log.debug('starting findSingle');
                    let fetched = yield users.findSingle(userid);
                    // added three tokens
                    _assertDeepEqual(user1MultiToken, fetched);
                    let result4 = yield users.clearAllLoginTokens(userid);
                    let removed  = yield users.findSingle(userid);
                    // removed all other tokens
                    _assertDeepEqual(user1, removed);
                    done();
                } catch (err) {
                    _throwCheckedExceptions(err);
                    log.debug("ERROR " + err.message );
                }

            });

            runtest(done);
        });

    });  // of clearAllLoginTokens

    // removeByUsername
    describe('removeByUsername', function () {
        it('should remove first user when there are two users', function (done) {
            let token1val = "mytoken123";
            let token2val = "my2ndtoken";
            let token3val = "my3rdtoken";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            let user2 = {
                username: "mary",
                emails: [{'address': 'mary@mary.com', verified: true}],
                profile: {testing: false}
            };
            var user1SingleToken = Object.assign({}, user1);
            user1SingleToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}]}};

            var user1MultiToken = Object.assign({}, user1);
            user1MultiToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val} ]}};


            let runtest = bluebird.coroutine(function* (done) {
                try {
                    log.debug('starting insertuser');
                    let userid1 = yield users.insertUser(user1);
                    log.debug('userid1 is ' + userid1);
                    let userid2 = yield users.insertUser(user2);
                    log.debug('userid2 is ' + userid2);

                    let res1 = yield users.removeByUsername('ken');
                    log.debug('res1 is ' + JSON.stringify(res1));
                    let found = yield users.findSingle(userid1);

                    log.debug('found is ' + JSON.stringify(found));
                    assert.isUndefined(found);
                    done();
                } catch (err) {
                    _throwCheckedExceptions(err);
                    log.debug("ERROR " + err.message );
                }

            });

            runtest(done);
        });

    });  // of removeByUsername

    // unsetProfile
    describe('unsetProfile', function () {
        it('should remove first user\'s profile and username when there are two users', function (done) {

            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            let user2 = {
                username: "mary",
                emails: [{'address': 'mary@mary.com', verified: true}],
                profile: {testing: false}
            };


            let runtest = bluebird.coroutine(function* (done) {
                try {
                    log.debug('starting insertuser');
                    let userid1 = yield users.insertUser(user1);
                    log.debug('userid1 is ' + userid1);
                    let userid2 = yield users.insertUser(user2);
                    log.debug('userid2 is ' + userid2);

                    let res1 = yield users.unsetProfile(userid1);
                    log.debug('res1 is ' + JSON.stringify(res1));

                    let found = yield users.findSingle(userid1);

                    log.debug('found is ' + JSON.stringify(found));
                    assert.isNull(found.profile);   // profile always defined
                    assert.isUndefined(found.username);
                    done();
                } catch (err) {
                    _throwCheckedExceptions(err);
                    log.debug("ERROR " + err.message );
                }

            });

            runtest(done);
        });

    });  // of unsetProfile

    // expireTokens
    describe('expireTokens', function () {
        it('should expire old tokens and leave new ones alone', function (done) {

            let oldToken1 = "mytoken123";
            let oldToken2 = "my2ndtoken";
            let newToken1 = "my3rdtoken";
            let newToken2 = "my4thtoken";

            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };


            var userNewTokenOnly = Object.assign({}, user1);
            userNewTokenOnly['services'] = {"resume": {"loginTokens": [{"hashedToken": newToken1},{"hashedToken": newToken2} ]}};




            let runtest = bluebird.coroutine(function* (done) {
                try {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('insert oldToken1');   // order matters in token array
                    let result1 = yield users.insertHLoginToken(userid, oldToken1, {});
                    log.debug('insert oldToken2');
                    let result2 = yield users.insertHLoginToken(userid, oldToken2, {});
                    log.debug('insert newToken1');
                    let result3 = yield users.insertHLoginToken(userid, newToken1, {});

                    // grab newToken1 timestamp as expTime
                    let firstfetch  = yield users.findSingle(userid);

                    let expTime = firstfetch.services.resume.loginTokens[2].when['$date'];
                    // grab date of last token
                    log.debug("expire-date is " + JSON.stringify(expTime));

                    log.debug('insert newToken2');
                    let result4 = yield users.insertHLoginToken(userid, newToken2, {});


                    let result5 = yield users.expireTokens(expTime,userid);

                    log.debug('result of delete is ' + JSON.stringify(result5));
                    let finalfetch = yield users.findSingle(userid);

                    log.debug('final user is ' + JSON.stringify(finalfetch));

                    _assertDeepEqual(userNewTokenOnly, finalfetch );


                    done();
                } catch (err) {
                    _throwCheckedExceptions(err);
                    log.debug("ERROR " + err.message );
                }

            });

            runtest(done);
        });

        it('should expire for all users if no userid specified', function (done) {

            let u1oldToken1 = "mytoken123";
            let u1oldToken2 = "my2ndtoken";
            let u1newToken1 = "my3rdtoken";
            let u1newToken2 = "my4thtoken";

            let u2oldToken1 = "u2token123";
            let u2oldToken2 = "u22ndtoken";
            let u2newToken1 = "u33rdtoken";

            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };

            let user2 = {
                username: "mary",
                emails: [{'address': 'mary@mary.com', verified: true}],
                profile: {testing: false}
            };

            var user1NewTokenOnly = Object.assign({}, user1);
            user1NewTokenOnly['services'] = {"resume": {"loginTokens": [{"hashedToken": u1newToken1},{"hashedToken": u1newToken2} ]}};

            var user2NewTokenOnly = Object.assign({}, user2);
            user2NewTokenOnly['services'] = {"resume": {"loginTokens": [{"hashedToken": u2newToken1} ]}};

            let runtest = bluebird.coroutine(function* (done) {
                try {
                    log.debug('insert user 1');
                    let user1id = yield users.insertUser(user1);
                    log.debug('insert user 2');
                    let user2id = yield users.insertUser(user2);
                    log.debug('insert oldToken1');   // order matters in token array
                    let result1 = yield users.insertHLoginToken(user1id, u1oldToken1, {});
                    log.debug('insert oldToken2');
                    let result2 = yield users.insertHLoginToken(user1id, u1oldToken2, {});

                    log.debug('insert u2 oldToken1');   // order matters in token array
                    let result3 = yield users.insertHLoginToken(user2id, u2oldToken1, {});
                    log.debug('insert u2 oldToken2');
                    let result4 = yield users.insertHLoginToken(user2id, u2oldToken2, {});



                    log.debug('insert newToken1');
                    let result5 = yield users.insertHLoginToken(user1id, u1newToken1, {});

                    // grab newToken1 timestamp as expTime
                    let firstfetch  = yield users.findSingle(user1id);

                    log.debug('insert u2 newToken1');

                    let result6 = yield users.insertHLoginToken(user2id, u2newToken1, {});


                    let expTime = firstfetch.services.resume.loginTokens[2].when['$date'];
                    // grab date of last token
                    log.debug("expire-date is " + JSON.stringify(expTime));

                    log.debug('insert newToken2');
                    let result7 = yield users.insertHLoginToken(user1id, u1newToken2, {});

                    // without user id
                    let result8 = yield users.expireTokens(expTime);

                    log.debug('result of delete is ' + JSON.stringify(result8));
                    let finalfetch = yield users.findSingle(user1id);

                    log.debug('final user 1 is ' + JSON.stringify(finalfetch));

                    _assertDeepEqual(user1NewTokenOnly, finalfetch );

                    let final2fetch = yield users.findSingle(user2id);

                    log.debug('final user 2 is ' + JSON.stringify(final2fetch));
                    _assertDeepEqual(user2NewTokenOnly, final2fetch );

                    done();
                } catch (err) {
                    _throwCheckedExceptions(err);
                    log.debug("ERROR " + err.message );
                }

            });

            runtest(done);
        });

    });  // of expireTokens



    // findUserWithNewOrOld
    describe('findUserWithNewOrOld', function () {
        it('should find user with new token only', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

            var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            // user coroutine to simplify logic
            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting isnertHLoginToken');
                let result = yield users.insertHLoginToken(userid, tokenval, {});
                log.debug('starting findUserWithNewOrOld');
                let fetched = yield users.findUserWithNewOrOld(tokenval);
                _assertDeepEqual(user1LoggedIn, fetched);
                done();

            });

            runtest(done);

        });

        it('should throw exception when old token specified', function (done) {
            let tokenval = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}, {
                    'address': 'flyerken@ken.com',
                    verified: true
                }],
                profile: {testing: true}
            };

            var user1LoggedIn = Object.assign({}, user1);
            user1LoggedIn['services'] = {"resume" :{"loginTokens":[{"hashedToken": tokenval}]}};

            // user coroutine to simplify logic
            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting isnertHLoginToken');
                let result = yield users.insertHLoginToken(userid, tokenval, {});
                log.debug('starting findUserWithNewOrOld');
                try {
                    let fetched = yield users.findUserWithNewOrOld(tokenval, "oldtoken");
                    // fail if no exception thrown
                    assert.equal(2,1);
                } catch(err) {
                    log.debug("Exception thrown " + err.message);
                    done();
                }


            });

            runtest(done);



        });

        }); //findUserWithNewOrOld



    // deleteSavedTokens
    describe('deleteSavedTokens', function () {
        it('should delete first and third token only', function (done) {
            let token1val = "mytoken123";
            let token2val = "my2ndtoken";
            let token3val = "my3rdtoken";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };

            var user1SingleToken = Object.assign({}, user1);
            user1SingleToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token2val}]}};



            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting first isnertHLoginToken');
                let result1 = yield users.insertHLoginToken(userid, token1val, {});
                log.debug('starting second isnertHLoginToken');
                let result2 = yield users.insertHLoginToken(userid, token2val, {});
                log.debug('starting third isnertHLoginToken');
                let result3 = yield users.insertHLoginToken(userid, token3val, {});

                log.debug('delete first and third tokens');
                let result4 = yield users.deleteSavedTokens(userid, [token1val, token3val]);
                log.debug("AFTER DELETE " + JSON.stringify(result4));
                log.debug('starting findSingle');
                let fetched = yield users.findSingle(userid);
                // only 2nd token left
                _assertDeepEqual(user1SingleToken, fetched);
                done();

            });

            runtest(done);
        });

    }); // deleteSavedTokens

        //  saveTokenAndDeleteLater
        describe('saveTokenAndDeleteLater', function () {
            it('should save all tokens to be deleted plus set new login token', function (done) {
                let token1val = "mytoken123";
                let token2val = "my2ndtoken";
                let token3val = "my3rdtoken";
                let currenttok = "logintoken";

                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };

                var user1Expected = Object.assign({}, user1);
                user1Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val},{"hashedToken": currenttok} ],
                    "haveLoginTokensToDelete": true,"loginTokensToDelete":[token1val, token2val, token3val] }};



                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('starting first isnertHLoginToken');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});

                    log.debug('save tokens to be deleted and save login token');
                    let result4 = yield users.saveTokenAndDeleteLater(userid, [token1val, token2val, token3val],currenttok);
                    log.debug("AFTER SAVE TOKENS " + JSON.stringify(result4));
                    log.debug('starting findSingle');
                    let fetched = yield users.findSingle(userid);
                    // all three tokens to delete plus the new login token
                    _assertDeepEqual(user1Expected, fetched);
                    done();

                });

                runtest(done);
            });

        }); // saveTokenAndDeleteLater


        // deleteSavedTokensforAllUsers
        describe('deleteSavedTokensforAllUsers', function () {
            it('should deleteSavedTokens for one user', function (done) {
                let token1val = "mytoken123";
                let token2val = "my2ndtoken";
                let token3val = "my3rdtoken";
                let currenttok = "logintoken";

                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };

                var user1Expected = Object.assign({}, user1);
                user1Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val},{"hashedToken": currenttok} ],
                    "haveLoginTokensToDelete": true,"loginTokensToDelete":[token1val, token2val, token3val] }};

                var user1AfterDelete = Object.assign({}, user1);
                user1AfterDelete['services'] = {"resume": {"loginTokens": [{"hashedToken": currenttok}]}};


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('starting first isnertHLoginToken');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});

                    log.debug('save tokens to be deleted later and set login token');
                    let result4 = yield users.saveTokenAndDeleteLater(userid, [token1val, token2val, token3val],currenttok);
                    log.debug("AFTER SAVE TOKENS " + JSON.stringify(result4));
                    log.debug('starting findSingle');
                    let fetched = yield users.findSingle(userid);
                    // all three tokens to delete plus the new login token
                    _assertDeepEqual(user1Expected, fetched);

                    let result5 = yield users.deleteSavedTokensforAllUsers();
                    log.debug("AFTER DELETE " + JSON.stringify(result5));
                    log.debug('starting findSingle');

                    let deleted = yield users.findSingle(userid);
                    _assertDeepEqual(user1AfterDelete, deleted);
                    done();

                });

                runtest(done);
            });

            it('should deleteSavedTokens for more than one user', function (done) {
                let token1val = "mytoken123";
                let token2val = "my2ndtoken";
                let token3val = "my3rdtoken";
                let currenttok = "logintoken";



                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };

                var user1Expected = Object.assign({}, user1);
                user1Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val},{"hashedToken": token2val},{"hashedToken": token3val},{"hashedToken": currenttok} ],
                    "haveLoginTokensToDelete": true,"loginTokensToDelete":[token1val, token2val, token3val] }};

                var user1AfterDelete = Object.assign({}, user1);
                user1AfterDelete['services'] = {"resume": {"loginTokens": [{"hashedToken": currenttok}]}};

                let u2token1val = "u2token123";
                let u2token2val = "u22ndtoken";
                let u2token3val = "u23rdtoken";
                let u2currenttok = "u2logintoken";


                let user2 = {
                    username: "mary",
                    emails: [{'address': 'mary@mary.com', verified: true}],
                    profile: {testing: false}
                };

                var user2Expected = Object.assign({}, user2);
                user2Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": u2token1val},{"hashedToken": u2token2val},{"hashedToken": u2token3val},{"hashedToken": u2currenttok} ],
                    "haveLoginTokensToDelete": true,"loginTokensToDelete":[u2token1val, u2token2val, u2token3val] }};

                var user2AfterDelete = Object.assign({}, user2);
                user2AfterDelete['services'] = {"resume": {"loginTokens": [{"hashedToken": u2currenttok}]}};


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('starting first isnertHLoginToken');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});

                    log.debug('save tokens to be deleted later and set login token');
                    let result4 = yield users.saveTokenAndDeleteLater(userid, [token1val, token2val, token3val],currenttok);

                    log.debug('starting user2');
                    let user2id = yield users.insertUser(user2);
                    log.debug('starting first isnertHLoginToken');
                    let result5 = yield users.insertHLoginToken(user2id, u2token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result6 = yield users.insertHLoginToken(user2id, u2token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result7 = yield users.insertHLoginToken(user2id, u2token3val, {});

                    log.debug('save tokens to be deleted later and set login token');
                    let result8 = yield users.saveTokenAndDeleteLater(user2id, [u2token1val, u2token2val, u2token3val],u2currenttok);


                    log.debug("AFTER SAVE TOKENS " + JSON.stringify(result8));
                    log.debug('starting findSingle for u1');
                    let fetched = yield users.findSingle(userid);
                    // all three tokens to delete plus the new login token
                    _assertDeepEqual(user1Expected, fetched);

                    log.debug('starting findSingle for u2');
                    let fetched2 = yield users.findSingle(user2id);
                    // all three tokens to delete plus the new login token
                    _assertDeepEqual(user2Expected, fetched2);

                    let result9 = yield users.deleteSavedTokensforAllUsers();
                    log.debug("AFTER DELETE " + JSON.stringify(result9));
                    log.debug('starting findSingle for u1');

                    let deleted = yield users.findSingle(userid);
                    _assertDeepEqual(user1AfterDelete, deleted);

                    log.debug('starting findSingle for u2');

                    let deleted2 = yield users.findSingle(user2id);
                    _assertDeepEqual(user2AfterDelete, deleted2);
                    done();

                });

                runtest(done);
            });


        }); // deleteSavedTokensforAllUsers



    // getNewToken
    describe('getNewToken', function () {
        it('should replace with new token but keep old token time', function (done) {
            let oldtokenValue = "oldtok";
            let newtokenValue = "newtok";

            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting first isnertHLoginToken');
                let result1 = yield users.insertHLoginToken(userid, oldtokenValue, {});

                log.debug('get user1 token and time');
                let found = yield users.findSingle(userid);
                let token = found.services.resume.loginTokens[0].hashedToken;
                let oldtime = found.services.resume.loginTokens[0].created_at;
                assert.equal(oldtokenValue, token);

                log.debug('get new token');
                let result2 = yield users.getNewToken(userid, oldtokenValue, newtokenValue);

                log.debug('get user1 token and time again');
                let found2 = yield users.findSingle(userid);
                let newtoken = found2.services.resume.loginTokens[0].hashedToken;
                let newtime = found2.services.resume.loginTokens[0].created_at;
                assert.equal(newtokenValue, newtoken);
                assert.equal(oldtime, newtime);

                done();
            });

            runtest(done);

        });

    });  // getNewToken



    // tests for not implemented (coversion) methods

    // addNewHashedTokenIfOldUnhashedStillExists
        describe('addNewHashedTokenIfOldUnhashedStillExists', function () {
            it('should throw exception when called', function (done) {

                try {
                    users.addNewHashedTokenIfOldUnhashedStillExists("id", "oldtoken", "newtoken", "when")
                        .then(function () {
                            // should not be here
                            log.debug('should not be here');
                            // force assertion error
                            assert.equal(1, 2);
                        });
                }catch(err) {
                    // expected a throw
                    done();
                }


            });

        }); // of addNewHashedTokenIfOldUnhashedStillExists

    // removeOldTokenAfterAddingNew

    describe('removeOldTokenAfterAddingNew', function () {
        it('should throw exception when called', function (done) {

            try {
                users.removeOldTokenAfterAddingNew("id", "oldtoken")
                    .then(function () {
                        // should not be here
                        log.debug('should not be here');
                        // force assertion error
                        assert.equal(1, 2);
                    });
            }catch(err) {
                // expected a throw
                done();
            }


        });

    }); // of removeOldTokenAfterAddingNew

    // findSingleLoggedIn
    describe('findSingleLoggedIn', function () {
        it('should find a logged on user', function (done) {

            let token1val = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            var user1Expected = Object.assign({}, user1);
            user1Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}]}};

            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('starting first isnertHLoginToken');
                let result1 = yield users.insertHLoginToken(userid, token1val, {});

                log.debug('get user1 token and time');
                let found = yield users.findSingleLoggedIn(userid);

                _assertDeepEqual(user1Expected, found);

                done();
            });

            runtest(done);


        });
        it('should not find a user not yet logged in', function (done) {
            let token1val = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };

            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);

                log.debug('get user1 token and time');
                let found = yield users.findSingleLoggedIn(userid);

                assert.isNull(found);

                done();
            });

            runtest(done);
        });

        it('should not find a logged out user', function (done) {
            let token1val = "mytoken123";
            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            var user1Expected = Object.assign({}, user1);
            user1Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}]}};

            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('log user in');
                let result1 = yield users.insertHLoginToken(userid, token1val, {});
                log.debug('log user out');
                let result2 = yield users.destroyToken(userid, token1val);

                log.debug('get user1');
                let found = yield users.findSingleLoggedIn(userid);

                assert.isNull(found);

                done();
            });

            runtest(done);

        });

        it('should  find a logged in user among several', function (done) {

            let token1val = "mytoken123";
            let token3val = "u3token123";
            let token4val = "u4token123";

            let user1 = {
                username: "ken",
                emails: [{'address': 'ken@mary.com', verified: false}],
                profile: {testing: true}
            };
            let user2 = {
                username: "mary",
                emails: [{'address': 'mary@mary.com', verified: true}],
                profile: {testing: true}
            };

            let user3 = {
                username: "john",
                emails: [{'address': 'john@mary.com', verified: false}],
                profile: {testing: true}
            };

            let user4 = {
                username: "karen",
                emails: [{'address': 'karen@mary.com', verified: true}],
                profile: {testing: true}
            };

            var user4Expected = Object.assign({}, user4);
            user4Expected['services'] = {"resume": {"loginTokens": [{"hashedToken": token4val}]}};
            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let user1id = yield users.insertUser(user1);
                let user2id = yield users.insertUser(user2);
                let user3id = yield users.insertUser(user3);
                let user4id = yield users.insertUser(user4);

                log.debug('log user 1 in');
                let result1 = yield users.insertHLoginToken(user1id, token1val, {});
                log.debug('log user 3 in');
                let result2 = yield users.insertHLoginToken(user3id, token3val, {});
                log.debug('log user 4 in');
                let result3 = yield users.insertHLoginToken(user4id, token4val, {});


                log.debug('log user 3 out');
                let result4 = yield users.destroyToken(user3id, token3val);


                log.debug('get user4');
                let found = yield users.findSingleLoggedIn(user4id);


                _assertDeepEqual(user4Expected, found);

                done();
            });

            runtest(done);

        });
    });  //findSingleLoggedIn


        //   setUsername(userid, username) {
        describe('setUsername', function () {
            it('should replace an existing username', function (done) {

                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };
                let user1Expected = {
                    username: "john",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);

                    let result1 = yield users.setUsername(userid, "john");


                    log.debug('get user1 ');
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);


            });

            it('should set a non-existent username', function (done) {

                let user1 = {
                    profile: {testing: true}
                };
                let user1Expected = {
                    username: "john",
                    profile: {testing: true}
                };
                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);

                    let result1 = yield users.setUsername(userid, "john");


                    log.debug('get user1 ');
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });
                runtest(done);
            });
        }); // setUsername


        //   findByResetToken(token) {
        describe('findByResetToken', function () {
            it('it should find user by reset token', function (done) {

                let token1val = "mytoken123";
                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };


                var user1Expected = Object.assign({}, user1);
                user1Expected.services.password.reset = {token: token1val};

                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set the reset token');

                    let result1 = yield users.setResetToken(userid, user1Expected.services.password.reset);

                    let found = yield users.findByResetToken(user1Expected.services.password.reset);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);


            });
        }); // findByResetToken


        //   findByVerificationTokens(token) {
        describe('findByVerificationTokens', function () {
            it('should find a user by verification token', function (done) {
                let token1val = "mytoken123";
                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}

                };

                var user1Expected = Object.assign({}, user1);
                user1Expected.services = {email: {verificationTokens: [{token: token1val, address: 'ken@mary.com'}]}};

                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set the reset token');

                    let result1 = yield users.setVerificationTokens(userid, user1Expected.services.email.verificationTokens[0]);

                    let found = yield users.findByVerificationTokens(user1Expected.services.email.verificationTokens[0]);
                    log.debug('FOUND = ' + JSON.stringify(found));
                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);


            });
        }); // findByVerificationTokens


        //   updateToBcrypt(userid, salted) {
        describe('updateToBcrypt', function () {
            it('should throw exception when called', function (done) {

                try {
                    users.updateToBcrypt("id", "salted")
                        .then(function () {
                            // should not be here
                            log.debug('should not be here');
                            // force assertion error
                            assert.equal(1, 2);
                        });
                } catch (err) {
                    // expected a throw
                    done();
                }


            });
        }); // updateToBcrypt


        //    replaceAllTokensOtherThanCurrentConnection(userId, hashed, currentToken) {
        describe('replaceAllTokensOtherThanCurrentConnection', function () {
            it('should replace all tokens other than current and set password', function (done) {

                let token1val = "mytoken123";
                let token2val = "my2ndtoken";
                let token3val = "my3rdtoken";
                let passwordval = "mypassword";
                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true}
                };

                var user1SingleToken = Object.assign({}, user1);
                user1SingleToken['services'] = {
                    "resume": {"loginTokens": [{"hashedToken": token2val}]},
                    password: {'bcrypt': passwordval}
                };

                var user1MultiToken = Object.assign({}, user1);
                user1MultiToken['services'] = {"resume": {"loginTokens": [{"hashedToken": token1val}, {"hashedToken": token2val}, {"hashedToken": token3val}]}};


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('starting first isnertHLoginToken');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});
                    log.debug('starting second isnertHLoginToken');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});
                    log.debug('starting third isnertHLoginToken');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});

                    log.debug('starting findSingle');
                    let fetched = yield users.findSingle(userid);
                    // added three tokens
                    _assertDeepEqual(user1MultiToken, fetched);
                    let result4 = yield users.replaceAllTokensOtherThanCurrentConnection(userid, passwordval, token2val);
                    let removed = yield users.findSingle(userid);
                    // removed all other tokens
                    _assertDeepEqual(user1SingleToken, removed);
                    done();

                });

                runtest(done);

            });
        }); // replaceAllTokensOtherThanCurrentConnection


        //    changePasswordResetVerifyEmail(userid, email, token, hashed) {
        describe('changePasswordResetVerifyEmail', function () {
            it('should update existing password and set verified flag', function (done) {

                let token1val = "mytoken123";
                let passwordval = "thisismypaswd";

                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };


                var user1WithReset = Object.assign({}, user1);
                user1WithReset.services.password.reset = {token: token1val};

                let user1Expected = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: true}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': passwordval}}
                };


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set the reset token');

                    let result1 = yield users.setResetToken(userid, user1WithReset.services.password.reset);

                    let resutl2 = yield users.changePasswordResetVerifyEmail(userid, user1.emails[0].address, token1val, passwordval);

                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);

            });
        }); // changePasswordResetVerifyEmail

        //    setVerificationTokens(userId, tokenRecord) {
        describe('setVerificationTokens', function () {
            it('should set verification token for associated email', function (done) {

                let token1val = "mytoken123";
                let emailval = "ken@mary.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': emailval, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };
                let verificationTokenRecord = {token: token1val, address: emailval};
                let user1WithToken = Object.assign({}, user1);
                user1WithToken.emails = user1.emails.slice(); // clone the array
                user1WithToken.services = Object.assign({}, user1.services);  // copy on write - adhoc deepcopy
                user1WithToken.services.email = {verificationTokens: [verificationTokenRecord]};

                user1WithToken.emails[0].verified = true;

                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set the verfication token');

                    let result1 = yield users.setVerificationTokens(userid, verificationTokenRecord);
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1WithToken, found);

                    done();
                });

                runtest(done);


            });
        }); // setVerificationTokens


        //    setResetToken(userId, tokenRecord) {
        describe('setResetToken', function () {
            it('should set password reset token for user', function (done) {

                let token1val = "mytoken123";
                let emailval = "ken@mary.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': emailval, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };
                let tokenRecord = {token: token1val};
                let user1WithResetToken = Object.assign({}, user1);
                user1WithResetToken.services.password.reset = tokenRecord;


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set the password reset token');

                    let result1 = yield users.setResetToken(userid, tokenRecord);
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1WithResetToken, found);

                    done();
                });

                runtest(done);

            });
        }); // setResetToken


        //    setEmailVerified(userid, addr) {
        describe('setEmailVerified', function () {
            it('should mark an email as verified', function (done) {

                let token1val = "mytoken123";
                let emailval = "ken@mary.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': emailval, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };

                let user1WithEmailVerified = Object.assign({}, user1);
                user1WithEmailVerified.emails[0].verified = true;


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set email verified');

                    let result1 = yield users.setEmailVerified(userid, emailval);
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1WithEmailVerified, found);

                    done();
                });

                runtest(done);


            });
        }); // setEmailVerified


    //   addEmailVerified(userId, email, verified) {
    describe('addEmailVerified', function () {
        it('should add an email verified record', function (done) {


            let token1val = "mytoken123";
            let email1val = "ken@mary.com";
            let email2val = "ken@ken.com";
            let email3val = "kennith@yahoo.com";

            let emailnew = "newmail@new.com";

            let user1 = {
                username: "ken",
                emails: [{'address': email1val, verified: false}, {
                    'address': email2val,
                    verified: false
                }, {'address': email3val, verified: false}],
                profile: {testing: true},
                services: {password: {'bcrypt': 'xxxooo323232'}}
            };

            let user1WithEmailVerified = Object.assign({}, user1);
            user1WithEmailVerified.emails = user1.emails.slice();
            user1WithEmailVerified.emails.push({'address': emailnew, 'verified': true});



            let runtest = bluebird.coroutine(function* (done) {
                log.debug('starting insertuser');
                let userid = yield users.insertUser(user1);
                log.debug('set email verified');

                let result1 = yield users.addEmailVerified(userid, emailnew, true);
                let found = yield users.findSingle(userid);

                _assertDeepEqual(user1WithEmailVerified, found);

                done();
            });

            runtest(done);


        });
    }); // addEmailVerified


        //   setNewEmailVerified(userid, email, newEmail, verified) {
        describe('setNewEmailVerified', function () {
            it('should replace an existing email address and verified status', function (done) {

                let email1val = "ken@mary.com";
                let email2val = "ken@ken.com";
                let email3val = "kennith@yahoo.com";

                let newemailval = "newken@supernew.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': email1val, verified: false}, {
                        'address': email2val,
                        verified: false
                    }, {'address': email3val, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };

                let user1WithEmailVerified = Object.assign({}, user1);
                user1WithEmailVerified.emails[1] = {'address': newemailval, 'verified': true};


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set email verified');

                    let result1 = yield users.setNewEmailVerified(userid, email2val, newemailval, true);
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1WithEmailVerified, found);

                    done();
                });

                runtest(done);


            });
        }); // setNewEmailVerified


        //   addEmailAddressVerified(userid, newEmail, verified) {
        describe('addEmailAddressVerified', function () {
            it('should add a new entry to array of emails with specified verified status', function (done) {

                let email1val = "ken@mary.com";
                let email2val = "ken@ken.com";
                let email3val = "kennith@yahoo.com";

                let newemailval = "newken@supernew.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': email1val, verified: false}, {
                        'address': email2val,
                        verified: false
                    }, {'address': email3val, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };

                let user1WithEmailVerified = Object.assign({}, user1);
                user1WithEmailVerified.emails = user1.emails.slice(); // copy-on-write ad-hoc deep clone
                user1WithEmailVerified.emails.push({'address': newemailval, 'verified': true});


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('set email verified');

                    let result1 = yield users.addEmailAddressVerified(userid, newemailval, true);
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1WithEmailVerified, found);

                    done();
                });

                runtest(done);


            });
        }); //addEmailAddressVerified


        //   forceChangePassword(userid, hashedPassword, loggedout) {
        describe('forceChangePassword', function () {
            it('should change password if logged in', function (done) {

                let token1val = "mytoken123";
                let newpassword = "newpassword";
                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };

                var user1Expected = Object.assign({}, user1);
                user1Expected.services = Object.assign({}, user1.services);
                user1Expected.services.resume = {"loginTokens": [{"hashedToken": token1val}]};
                user1Expected.services.password['bcrypt'] = newpassword;

                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('insert user');
                    let userid = yield users.insertUser(user1);
                    log.debug('log user in');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});

                    let result2 = yield users.forceChangePassword(userid, newpassword, false);

                    log.debug('get user1 token and time');
                    let found = yield users.findSingleLoggedIn(userid);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);


            });


            it('should change password and remove all login tokens if logged out', function (done) {

                let token1val = "mytoken123";
                let token2val = "secondtok";
                let token3val = "thirdtok";

                let newpassword = "newpassword";

                let user1 = {
                    username: "ken",
                    emails: [{'address': 'ken@mary.com', verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };
                var user1Expected = Object.assign({}, user1);
                user1Expected.services.password['bcrypt'] = newpassword;

                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('insert user');
                    let userid = yield users.insertUser(user1);
                    log.debug('add LoginToken');
                    let result1 = yield users.insertHLoginToken(userid, token1val, {});

                    log.debug('add LoginToken 2');
                    let result2 = yield users.insertHLoginToken(userid, token2val, {});

                    log.debug('add LoginToken 3');
                    let result3 = yield users.insertHLoginToken(userid, token3val, {});


                    log.debug('logged out');
                    let result4 = yield users.forceChangePassword(userid, newpassword, true);


                    log.debug('get user1 token and time');
                    let found = yield users.findSingle(userid);

                    _assertDeepEqual(user1Expected, found);

                    done();
                });

                runtest(done);


            });

        }); //forceChangePassword

        //    undoEmailAddressUpdate(userid, newEmail) {
        describe('undoEmailAddressUpdate', function () {
            it('should remove an email just added', function (done) {

                let email1val = "ken@mary.com";
                let email2val = "ken@ken.com";
                let email3val = "kennith@yahoo.com";

                let newemailval = "newken@supernew.com";

                let user1 = {
                    username: "ken",
                    emails: [{'address': email1val, verified: false}, {
                        'address': email2val,
                        verified: false
                    }, {'address': email3val, verified: false}],
                    profile: {testing: true},
                    services: {password: {'bcrypt': 'xxxooo323232'}}
                };

                let user1WithEmailVerified = Object.assign({}, user1);
                user1WithEmailVerified.emails = user1.emails.slice();  // copy the array
                user1WithEmailVerified.emails.push({'address': newemailval, 'verified': true});


                let runtest = bluebird.coroutine(function* (done) {
                    log.debug('starting insertuser');
                    let userid = yield users.insertUser(user1);
                    log.debug('USER 1 is ' + JSON.stringify(user1));
                    log.debug('set email verified');

                    let result1 = yield users.addEmailAddressVerified(userid, newemailval, true);
                    let found = yield users.findSingle(userid);
                    log.debug("ASSERT ADDRESS UPDATED");
                    _assertDeepEqual(user1WithEmailVerified, found);
                    log.debug("ASSERT ADDRESS UNDO");
                    /* let result2 = yield users.undoEmailAddressUpdate(userid, newemailval);*/
                    let found2 = yield users.findSingle(userid);

                    _assertDeepEqual(user1, found2);


                    done();
                });

                runtest(done);


            });
        }); // undoEmailAddressUpdate

    // setSRP
    describe('setSRP', function () {
        it('should throw exception when called', function (done) {

            try {
                users.setSRP("userId", "identity", "salt", "verifier")
                    .then(function () {
                        // should not be here
                        log.debug('should not be here');
                        // force assertion error
                        assert.equal(1, 2);
                    });
            }catch(err) {
                // expected a throw
                done();
            }


        });

    }); // of setSRP



        });  // of Accounts

