'use strict';

// not in ES6 - avoid having to install babel-node just for this file
var promise = require('bluebird'); // or any other Promise/A+ compatible library;
var options = {
    promiseLib: promise // overriding the default (ES6 Promise);
};
var pgp = require('pg-promise')(options);
var fs = require('fs');
var conf = require('./config/conf');

var cn = conf.get();

var db = pgp(cn); // database instance;


var sqlstmts = fs.readFileSync("./schema/createtable.sql", "utf8");
console.log("content is " + sqlstmts);

db.result(sqlstmts, true)
    .then(function (result) {
        console.log("DATA:", JSON.stringify(result)); // print data;
    })
    .catch(function (error) {
        console.log("ERROR:", error); // print the error;
    })
    .finally(function () {
        pgp.end(); // for immediate app exit, closing the connection pool.


    });
