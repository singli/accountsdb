"use strict";
import  bluebird from 'bluebird';
import r from 'rethinkdb';
import log from 'loglevel';
import conf from '../config/conf';

// uncomment for verbose debug logs
//log.setLevel('debug');

export default class AccountRethink {
    constructor() {
        this.cn = conf.get();
        log.debug("in constructor");

    }
    // re-shape by removing any empty or falsey fields
    prune(user) {
        if (user.services) {

            if (user.services.password) {

                    if (user.services.password.reset === null) {
                        delete user.services.password.reset;
                        delete user.services.password.resetPending;
                    }
                    if ('resetPending' in user.services.password) {
                        if (user.services.password.resetPending == false) {
                            delete user.services.password.resetPending;
                        }
                    }

            }
            if (user.services.resume) {
                if (user.services.resume.loginTokensToDelete) {
                    if (user.services.resume.loginTokensToDelete.length == 0) {
                        delete user.services.resume.haveLoginTokensToDelete;
                        delete user.services.resume.loginTokensToDelete;
                    }
                }
                if (user.services.resume.loginTokens) {
                    if (user.services.resume.loginTokens.length == 0) {
                        delete user.services.resume;
                    }
                }
            }

            var keys = Object.keys(user.services);
            for (let key of keys) {
                if (user.services[key] == null) {
                    delete user.services[key];
                }
            }
            keys = Object.keys(user.services);
            if (keys.length == 0) {
                delete user['services'];
            }

        }
        return user;

    }


    insertUser(fullUser) {
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("got connection... ");
                myconn = conn;
                fullUser.created_at = new Date();

              return  r.table("users").insert(fullUser).run(conn)
                 .then( function(data) {
                     log.debug(JSON.stringify(data));

                     if (data.inserted === 1) {
                         return data.generated_keys[0];
                     }else {
                         // should throw error
                         return "ok";
                     }
                 })
                  .error(function(err) {
                      log.debug("ERROR " + err.message);
                  });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
            myconn.close();
        });


    }
    findSingle(userid) {
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find single - got connection... ");
                myconn = conn;

                return  r.table("users").get(userid).run(conn)
                    .then( function(user) {
                        if (!user) {
                            return undefined;
                        }
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }
    findByUsername(username) {
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find by user name - got connection... ");
                myconn = conn;

                return  r.table("users").filter({'username': username}).run(conn)
                    .then( function(found) {
                        var user = found.next();  // filter() returns a cursor
                        log.debug('found record is ' + user);
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    findCurrentUser(userid) {
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find current user - got connection... ");
                myconn = conn;

                return  r.table("users").get(userid).pluck('username','profile','emails').run(conn)
                    .then( function(user) {
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }



    findByService(svname, serviceid) {
        // always called with selector {services.<servicename>.id: "id"}

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find by service - got connection... ");
                myconn = conn;

                return  r.table("users").filter(function(row) { return row('services')(svname)('id').eq(serviceid);}).run(conn)
                    .then( function(found) {
                        var user = found.next();  // filter() returns a cursor
                        log.debug('found record is ' + user);
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    //  The following are mostly used in setup of package tests  - via TinyTest

    removeByUsername(username) {
        // delete the user based on username
        // return this.users.remove({username: username});

        //  TODO: setup cascade delete on the table basis
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In remove by username - got connection... ");
                myconn = conn;

                return  r.table("users").filter({'username': username}).delete().run(conn)
                    .then( function( result) {
                        log.debug('delete user result is ' + JSON.stringify(result));
                        return result.deleted;
                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    unsetProfile(userId) {

        // clear out the profile and username fields for the specified userId
        // this.users.update(userId,
        //    {$unset: {profile: 1, username: 1}});

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In unset profile - got connection... ");
                myconn = conn;

                return  r.table("users").get(userId).update({profile: null}).run(conn)
                    .then( function( result) {
                        log.debug('unset profile  result is ' + JSON.stringify(result));
                        return r.table("users").get(userId).replace(r.row.without('username')).run(conn)
                            .then( function(res2) {
                                return res2.replaced;
                            })
                            .error(function(err) {
                               log.debug("ERR " + err.message);
                            });
                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    findById(userId) {
        return this.findSingle(userId);
    }



    // working with login tokens

    expireTokens(oldestValidDate, userId) {
        // for a specified user, given the oldest valid date, expire all older token
        log.debug('oldestValideDate is ' + JSON.stringify(oldestValidDate));
        // if userId is null, expire token for everyone on system

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In unset profile - got connection... ");
                myconn = conn;
                if (!userId) {  // undefined userI - expire for everyone
                    return  r.table("users").update(function(row) {
                        return { services: {resume: {loginTokens: row('services')('resume')('loginTokens').filter( function(item) { return item('when')('$date').ge(oldestValidDate);}) }}};

                    }).run(conn)
                        .then( function( result) {
                            log.debug('expire token  result is ' + JSON.stringify(result));
                            return result;

                        })
                        .error(function(err) {
                            log.debug("ERROR " + err.message);
                        });

                }


                // when you have nested object and arrays - rethink ops can get tricky quickly
                return  r.table("users").get(userId).update(function(row) {
                    return { services: {resume: {loginTokens: row('services')('resume')('loginTokens').filter( function(item) { return item('when')('$date').ge(oldestValidDate);}) }}};

                    }).run(conn)
                    .then( function( result) {
                        log.debug('expire token  result is ' + JSON.stringify(result));
                        return result;

                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }


    clearAllLoginTokens(userId) {

        // delete all login token for a user
        // return this.db.none("delete from account_login_tokens where user_id=$1", [userId]);

        var myconn;
        return r.connect(this.cn)
            .then( conn => {
                log.debug("In clearalllogintokens - got connection... ");
                myconn = conn;
                // set resume to null,  this.prune() will take care of pruning null services
                return  r.table("users").get(userId).update({services:{resume:null}}).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    insertHLoginToken(userId, hashedToken, qry) {
        // log.debug("called insert login token " + userId + " --- " + JSON.stringify(hashedToken) + " query is " + JSON.stringify(qry));
        // test this.prune -- log.debug ("PRUNE " + JSON.stringify(this.prune({services: {resume: null, facebook: null, twitter: null}})));


        // insert a login token for a user
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In insertHLoginToken - got connection... ");
                myconn = conn;
                var tokenVal = {'hashedToken': hashedToken, 'when': {'$date': new Date() }};
                return  r.table("users").get(userId).hasFields({services: {resume: {loginTokens: true}}}).run(conn)
                   .then( function( hastokens) {
                       if (hastokens) {
                           log.debug('APPEND');
                           return  r.table('users').get(userId).update( function(row) {
                               return {services: {resume: {loginTokens: row('services')('resume')('loginTokens').append(tokenVal)}}};
                           }).run(conn);
                       }
                        else {
                           log.debug('UPDATE');
                           return r.table('users').get(userId).update({services: {resume: {loginTokens: [tokenVal]}}}).run(conn);

                       }


                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    findUserWithNewtoken(hashedToken) {
        // find the user with a specified token
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In findUserWithNewToken - got connection... ");
                myconn = conn;
                // set resume to null,  this.prune() will take care of pruning null services
                return r.table('users').filter(r.row('services')('resume')('loginTokens').contains(function(em) {return em('hashedToken').eq(hashedToken);})).run(conn)
                    .then( function( result) {
                        var user = result.next();
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });



    }

    findUserWithNewOrOld(newToken, oldToken) {
        // old token method
        // DO NOT support old token
        /*
         return  this.users.findOne({
         $or: [
         {"services.resume.loginTokens.hashedToken": newToken},
         {"services.resume.loginTokens.token": oldToken}
         ]
         });
         */
        if (oldToken) {
            var status = "WARNING:  findUserWithNewOrOld invoked with oldToken but not implemented";
            throw new Error(status);
        }
        return this.findUserWithNewtoken(newToken);
    }



    deleteSavedTokens(userId, tokensToDelete) {
        log.debug("DEL SAVED uid " + userId + " TOK " + JSON.stringify(tokensToDelete));
        // tokensToDelete must be an array of tokens
        if (tokensToDelete) {

            var myconn;
            return r.connect(this.cn)
                .then( function(conn) {
                    log.debug("In deleteSavedToken - got connection... ");
                    myconn = conn;
                    // clear tokens to delete
                    return r.table('users').get(userId).update({services: {resume: {haveLoginTokensToDelete:false, loginTokensToDelete: []}}}).run(conn)
                        .then( function( result) {

                            // remove  tokensToDelete
                            return r.table("users").get(userId).update(
                                { services: {resume: {loginTokens: r.row('services')('resume')('loginTokens').filter(
                                    function(row) {return r.expr(tokensToDelete).contains(row('hashedToken')).not(); })}}}).run(conn);

                        }.bind(this))
                        .error(function(err) {
                            log.debug("ERROR " + err.message);
                        });
                }.bind(this))
                .error(function(err) {
                    log.debug("ERR " + err.message);
                })
                .finally(function() {
                    myconn.close();
                });
        }
    }

    deleteSavedTokensforAllUsers() {
        // for each user with haveLoginTokensToDelete set to true
        // delete the list of tokens stored in loginTokensToDelete

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In deleteSavedTokensForAllUsers - got connection... ");
                myconn = conn;

                return  r.table("users").hasFields({services:{resume:{haveLoginTokensToDelete:true}}}).filter({services:{resume:{haveLoginTokensToDelete:true}}}).run(conn)
                    .then( (found) => {
                        // crazy syntax - returns a promise that is resolved only after each of the containing promises are resolved sequentially
                        return found.eachAsync((row) => {
                            log.debug("adding to ARRAY + " + row.id  + " tokens " + row.services.resume.loginTokensToDelete );
                            return this.deleteSavedTokens(row.id, row.services.resume.loginTokensToDelete);
                        });
                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }
    removeOtherTokens(userId, curToken) {
        // delete all other tokens for a user (excpect the specified curToken)

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In deleteSavedToken - got connection... ");
                myconn = conn;
                // remove  tokensToDelete
                return r.table("users").get(userId).update(
                { services: {resume: {loginTokens: r.row('services')('resume')('loginTokens').filter(
                     function(row) {return r.expr([curToken]).contains(row('hashedToken')); })}}}).run(conn);

            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    getNewToken(userId, curToken, newToken) {

        if (!userId) {
            throw new Error("You are not logged in");
        }


        // Be careful not to generate a new token that has a later
        // expiration than the curren token. Otherwise, a bad guy with a
        // stolen token could use this method to stop his stolen token from
        // ever expiring.

        // here, we do this by keeping the timestamp from the current token - and replace just the token string

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In getNewToken - got connection... ");
                myconn = conn;

                // find the index of old token, replaced with the new one merged in (keeping timestamp intact)
                return r.table("users").get(userId).update(function(row) {
                    return row('services')('resume')('loginTokens').offsetsOf(
                        function (x) {
                            return x('hashedToken').eq(curToken);
                        })(0)
                        .do(function (index) {
                            return {
                                services: {
                                    resume: {
                                        loginTokens: row('services')('resume')('loginTokens')
                                            .changeAt(index, row('services')('resume')('loginTokens')(index).merge({'hashedToken': newToken}))
                                    }
                                }
                            };
                        });
                }).run(conn);
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }


    addNewHashedTokenIfOldUnhashedStillExists(userId, oldToken, newHashedToken, when) {
        //  log.debug("called addNewHasedTokenIfOldUnhashedStillExists with userID " + userId + "oldToken= " + oldToken + "newToken =" + newHashedToken +  "when = " + when);
        // working with old tokens -- not implemented

        /*

         this.users.update({_id: userId,
         "services.resume.loginTokens.token": oldToken},
         {
         $addToSet: {
         "services.resume.loginTokens":
         {
         "hashedToken":  newHashedToken,
         "when":when
         }
         }});
         */

        var status = "WARNING:  addNewHashedTokenIfOldUnhashedStillExists is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);

    }


    removeOldTokenAfterAddingNew(userid, oldToken) {
        // log.debug("called removeOldTokenAfterAddingNew with userID " + userid + "oldToken= " + oldToken);
        /*
         this.users.update(userid, {
         $pull: {
         "services.resume.loginTokens": {"token": oldToken}
         }
         });
         */
        var status = "WARNING:  removeOldTokenAfterAddingNew is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);
    }

    destroyToken(userId, loginToken) {
        // remove specified token for specified userId
        log.debug("DESTROY token " + userId + " token is " + loginToken);
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In destroyToken - got connection... ");
                myconn = conn;
                // destroy  loginToken
                return r.table("users").get(userId).update(
                    { services: {resume: {loginTokens: r.row('services')('resume')('loginTokens').filter(
                        function(row) {return r.expr([loginToken]).contains(row('hashedToken')).not(); })}}}).run(conn);

            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    findSingleLoggedIn(userId) {
        // find the logged in user

        // if the sql service provider is coded correctly, this direct-from-mongo-code method
        // should "just work"
        return this.findSingle(userId)
            .then(user => {
                // for the lack of an existantial operator :(
                if(user) {

                    if(user.services) {
                        if (user.services.resume) {
                            if (user.services.resume.loginTokens) {
                                return bluebird.resolve(user);
                            } else {
                                return bluebird.resolve(null);
                            }
                        } else {
                            return bluebird.resolve(null);
                        }

                    } else {
                        return bluebird.resolve(null);
                    }

                } else {  //null
                    return bluebird.resolve(user);
                }


            })
            .catch(function(err) {
                log.debug("ERR " + err.message);
            });


    }

    saveTokenAndDeleteLater(userId, tokens, hashedStampedToken) {
        // For a specified userId,
        // tokens contains an array of tokens to delete later (token strings only)
        // hashedStampedToken is a token to be inserted into the loginTokens list now, creating it if not existing yet


        // TODO: need to combine the two ops below into one transaction
        // implemented as an upsert into the accout_services_resume table


        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In saveTokenAndDeleteLater - got connection... ");
                myconn = conn;
                // clear tokens to delete
                return r.table('users').get(userId).update({services: {resume: {haveLoginTokensToDelete:true, loginTokensToDelete:tokens}}}).run(conn)
                    .then( function( result) {

                        // remove  tokensToDelete
                        return this.insertHLoginToken(userId, hashedStampedToken, {});

                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });




    }

    // password_server requirements

    findByResetToken(tokenRec) {
        // returns the user with the supplied password reset token
        // return this.users.findOne({"services.password.reset.token": token});

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find by reset token - got connection... ");
                myconn = conn;

                return  r.table("users").filter({'services': {'password': {'reset':  tokenRec}} }).run(conn)
                    .then( function(found) {
                        var user = found.next();  // filter() returns a cursor
                        log.debug('found record is ' + user);
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    findByVerificationTokens(tokenRec) {
        // returns the user with the supplied email verification token
        // return this.users.findOne({"services.email.verificationTokens.token": token});

        log.debug("FIND BY VERIFICATIONTOKENS " + JSON.stringify(tokenRec));
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In find by verification token - got connection... ");
                myconn = conn;
                return r.table('users').filter(r.row('services')('email')('verificationTokens').contains(function(em) {return em('token').eq(tokenRec.token);})).run(conn)
                    .then( function(found) {
                        var user = found.next();  // filter() returns a cursor
                        log.debug('found record is ' + user);
                        return this.prune(user);
                    }.bind(this))
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    updateToBcrypt(userid, salted) {
        // working with old SRP passowrds -- not implemented
        /*
         this.users.update(userid, {$unset: { 'services.password.srp': 1 }, $set: { 'services.password.bcrypt': salted }});
         */

        var status = "WARNING:  updateToBcrypt is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);

    }

    setUsername(userId, username) {
        // change the username for a specified user
        // this.users.update({_id: userid}, {$set: {username: username}});
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In unset profile - got connection... ");
                myconn = conn;

                return  r.table("users").get(userId).update({'username': username}).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }

    replaceAllTokensOtherThanCurrentConnection(userId, hashed, currentToken) {
        // for specified user, set the encrypted password, remove all tokens except the currentToken, AND clear the reset status


        var myconn;
        return r.connect(this.cn)
            .then( (conn) => {
                log.debug("In replaceAllTokensOtherThanCurrentConnection - got connection... ");
                myconn = conn;
                return r.table("users").get(userId).update({'services': {'password': {'bcrypt': hashed}}}).run(conn)
                    .then((res) => {
                        return this.removeOtherTokens(userId, currentToken)
                            .then((res) => {
                                return r.table("users").get(userId).replace(r.row.without({service: {password: {reset: true}}})).run(conn);
                            });
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    changePasswordResetVerifyEmail(userId, email, token, hashed) {
        // for a specified user, email, and reset token combination - clear resetpending, set the bcrypt password, and set verified to true for corresponding email
        log.debug("TOKEN IN is " + JSON.stringify(token));
        let tokenRec = {'token': token};
        return bluebird.coroutine(function*() {
            let conn = yield r.connect(this.cn);
            let userfound = yield this.findByResetToken(tokenRec);
            if (userfound.id != userId) {
                throw new Error("token does not belong to user");
            }
            log.debug('not yet');
            let  res = yield r.table("users").get(userId).update({'services': {'password': {'bcrypt': hashed, 'resetPending': false, 'reset': null}}}).run(conn);
            log.debug('after');
            return  yield this.setEmailVerified(userId, email);


            conn.close();
        }.bind(this))();
    }

    setVerificationTokens(userId, tokenRecord) {
        //set the verification token for a user
        /* this.users.update(
         {_id: userId},
         {$push: {'services.email.verificationTokens': tokenRecord}}); */

        // insert a verification token for a user
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In setVerificationTokens - got connection... ");
                myconn = conn;
                var tokenVal = {'token': tokenRecord.token, 'when': {'$date': new Date() },  'address': tokenRecord.address};
                return  r.table("users").get(userId).hasFields({services: {email: {verificationTokens: true}}}).run(conn)
                    .then( function( hastokens) {
                        if (hastokens) {
                            log.debug('APPEND');
                            return  r.table('users').get(userId).update( function(row) {
                                return {services: {email: {verificationTokens: row('services')('email')('verificationTokens').append(tokenVal)}}};
                            }).run(conn);
                        }
                        else {
                            log.debug('UPDATE');
                            return r.table('users').get(userId).update({services: {email: {verificationTokens: [tokenVal]}}}).run(conn);

                        }


                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });



    }

    setResetToken(userId, tokenRecord) {
        // set the password reset token for a user
        /*
         this.users.update(userId, {
         $set: {
         "services.password.reset": tokenRecord
         }
         });
         */
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In set ResetToken - got connection... ");
                myconn = conn;

                return  r.table("users").get(userId).update({'services':{'password': {'reset':  tokenRecord} }}).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });
    }

    setEmailVerified(userId, addr) {
        // for a sepcified user id,  find the email - set verified status to true and remove the associated verification token
        /*
         this.users.update({_id: userid, 'emails.address': addr},
         {
         $set: {'emails.$.verified': true},
         $pull: {'services.email.verificationTokens': {address: addr}}
         });
         */
        // TODO:  verify if higher level logic expects to set anything but the primary email
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In set email verified - got connection... ");
                myconn = conn;


                return r.table("users").get(userId).update(function(row) {
                    return row('emails').offsetsOf(
                        function (x) {
                            return x('address').eq(addr);
                        })(0)
                        .do(function (index) {
                            return {
                                emails: row('emails')
                                            .changeAt(index, row('emails')(index).merge({'verified': true}))
                                    };


                        });
                }).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

    }

    addEmailVerified(userId, email, verified) {
        // for a specified user id, find the email - set verified status
        /*
         this.users.update(
         {_id: userId},
         {$push: {emails: {address: email, verified: verified}}});
         */
        // note verified can be true or false
        // TODO:  may have to convert this to an upsert
        // insert a verification token for a user
        log.debug('adding email in addEmailVerified ->' + email);
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In addEmailVerified - got connection... ");
                myconn = conn;
                return  r.table("users").get(userId).hasFields( 'emails').run(conn)
                    .then( function( emails) {
                        let addrRec = {'address': email, 'verified': verified};
                        if (emails) {
                            log.debug('APPEND');
                            return  r.table('users').get(userId).update( function(row) {
                                return  {emails: row('emails').append(addrRec)};
                            }).run(conn);
                        }
                        else {
                            log.debug('ADD');
                            return r.table('users').get(userId).update({emails: addrRec }).run(conn);

                        }


                    })
                    .error(function(err) {
                        log.debug("ERROR " + err.message);
                    });
            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });



    }

    setNewEmailVerified(userId, email, newEmail, verified) {

        // replace an existing email with a new email and verification status
        /*
         this.users.update({_id: userid, 'emails.address': email},
         {
         $set: {
         'emails.$.address': newEmail,
         'emails.$.verified': verified
         }
         });
         */

        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In set new email verified - got connection... ");
                myconn = conn;


                return r.table("users").get(userId).update(function(row) {
                    return row('emails').offsetsOf(
                        function (x) {
                            return x('address').eq(email);
                        })(0)
                        .do(function (index) {
                            return {
                                emails: row('emails')
                                    .changeAt(index, row('emails')(index).merge({'address': newEmail, 'verified': verified}))
                            };


                        });
                }).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });


    }


    addEmailAddressVerified(userid, newEmail, verified) {

        // add an email and verification status record
        /*
         this.users.update({_id: userid},
         {
         $addToSet: {
         emails: {
         address: newEmail,
         verified: verified
         }
         }
         });
         */

        return  this.addEmailVerified(userid, newEmail, verified);
     }

    forceChangePassword(userId, hashedPassword, loggedout) {

        /*
         var update = {
         $unset: {
         'services.password.srp': 1, // XXX COMPAT WITH 0.8.1.3
         'services.password.reset': 1
         },
         $set: {'services.password.bcrypt': hashedPassword }
         };

         if (loggedout) {
         update.$unset['services.resume.loginTokens'] = 1;
         }

         this.users.update({_id: userid}, update);
         */
        // for a specified user, set the password and make sure resetpending is false;  also clear loginTokens if loggedOut
        var myconn;
        return r.connect(this.cn)
            .then( conn => {
                log.debug("In set new email verified - got connection... ");
                myconn = conn;

                if(loggedout) {
                   return this.clearAllLoginTokens(userId)
                        .then(res => {
                            return r.table("users").get(userId).update({
                                services: {
                                    password: {
                                        bcrypt: hashedPassword,
                                        resetPending: false
                                    }
                                }
                            }).run(conn);
                        });
                }
                return r.table("users").get(userId).update({services: {password: {bcrypt: hashedPassword, resetPending: false}}}).run(conn);

            })
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });
   }

    undoEmailAddressUpdate(userId, newEmail) {
        // remove the email address specified

        // this.users.update({_id: userid}, {$pull: {emails: {address: newEmail}}});
        log.debug("undoEmailAddressUpdate userId " + userId + " email is " + newEmail);
        var myconn;
        return r.connect(this.cn)
            .then( function(conn) {
                log.debug("In undoEmailAddressUpdate - got connection... ");
                myconn = conn;
                // destroy  loginToken
                return r.table("users").get(userId).update(
                    {emails: r.row('emails').filter(
                        function(row) {return r.expr([newEmail]).contains(row('address')).not(); })}).run(conn);

            }.bind(this))
            .error(function(err) {
                log.debug("ERR " + err.message);
            })
            .finally(function() {
                myconn.close();
            });

     }

    setSRP(userId, identity, salt, verifier) {
        // working with old SRP passowrds -- not implemented
        /*
         this.users.update(userid, {$unset: { 'services.password.srp': 1 }, $set: { 'services.password.bcrypt': salted }});
         */
        /*
         this.users.update(
         userId,
         {
         '$set': {
         'services.password.srp': {
         "identity": identity,
         "salt": salt,
         "verifier": verifier
         }
         }
         });
         */
        var status = "WARNING:  setSRP() is invoked but not implemented";
        log.debug(status);
        throw new Error(status);
        return promise.resolve(true);

    }


    resetCollection() {
        log.debug('reset Collection!');
        // use rethink delete
            var myconn;
            return r.connect(this.cn)
                .then( function(conn) {
                    log.debug("In resetConnection - got connection... ");
                    myconn = conn;

                    return  r.table("users").delete().run(conn)
                        .then( function(status) {
                            log.debug(JSON.stringify(status));
                            return true;
                        })
                        .error(function(err) {
                            log.debug("ERROR " + err.message);
                        });
                })
                .error(function(err) {
                    log.debug("ERR " + err.message);
                })
                .finally(function() {
                    myconn.close();
                });

    }
}
