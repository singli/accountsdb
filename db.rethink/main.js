'use strict';

var r = require('rethinkdb');
var conf = require('./config/conf');

var connection = null;
r.connect( conf.get(), function(err, conn) {
    if (err) throw err;
    connection = conn;
    r.tableDrop('users').run(connection, function(err, result) {
        if (err)
          console.log('users does not exist');

        r.tableCreate('users').run(connection, function(err, result) {
            if (err) throw err;
            console.log(JSON.stringify(result, null, 2));
            conn.close();
        });
    });

})